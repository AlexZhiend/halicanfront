import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ConstantesFisiologicas } from '../_model/constantesfisiologicas';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConstantesfisiologicasService extends GenericService<ConstantesFisiologicas> {

  private ConstantesFisiologicasCambio = new Subject<ConstantesFisiologicas[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/constantesfisiologicas`);
  }

  //get Subjects
  getConstantesFisiologicasCambio() {
    return this.ConstantesFisiologicasCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setConstantesFisiologicasCambio(ConstantesFisiologicases: ConstantesFisiologicas[]) {
    this.ConstantesFisiologicasCambio.next(ConstantesFisiologicases);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
