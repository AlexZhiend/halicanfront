import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Cliente } from '../_model/cliente';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SearchConsultaDTO } from '../_model/searchConsultaDTO';

@Injectable({
  providedIn: 'root'
})
export class ClienteService extends GenericService<Cliente> {

  private ClienteCambio = new Subject<Cliente[]>();
  private mensajeCambio = new Subject<string>();

  private clientebuscado = new Subject<Cliente>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/cliente`);
  }

  getClienteadd(){
    return this.clientebuscado.asObservable()
  }

  setClienteadd(clienteseleccionado: Cliente) {
    this.clientebuscado.next(clienteseleccionado);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  BuscarNombreDNI(searchConsultaDTO: SearchConsultaDTO) {
    return this.http.post<Cliente[]>(`${this.url}/buscar/nombreodni`, searchConsultaDTO);
  }

  //get Subjects
  getClienteCambio() {
    return this.ClienteCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setClienteCambio(Clientees: Cliente[]) {
    this.ClienteCambio.next(Clientees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
