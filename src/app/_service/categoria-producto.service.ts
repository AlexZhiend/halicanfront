import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CategoriaProducto } from '../_model/categoriaproducto';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriaProductoService extends GenericService<CategoriaProducto> {

  private CategoriaProductoCambio = new Subject<CategoriaProducto[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/categoriaproducto`);
  }

  //get Subjects
  getCategoriaProductoCambio() {
    return this.CategoriaProductoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setCategoriaProductoCambio(CategoriaProductoes: CategoriaProducto[]) {
    this.CategoriaProductoCambio.next(CategoriaProductoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
