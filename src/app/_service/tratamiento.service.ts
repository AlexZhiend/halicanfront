import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Tratamiento } from '../_model/tratamiento';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TratamientoService extends GenericService<Tratamiento> {

  private TratamientoCambio = new Subject<Tratamiento[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tratamiento`);
  }

  //get Subjects
  getTratamientoCambio() {
    return this.TratamientoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTratamientoCambio(Tratamientoes: Tratamiento[]) {
    this.TratamientoCambio.next(Tratamientoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

}
