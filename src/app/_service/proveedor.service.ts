import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Proveedor } from '../_model/proveedor';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService extends GenericService<Proveedor> {

  private ProveedorCambio = new Subject<Proveedor[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/proveedor`);
  }

  //get Subjects
  getProveedorCambio() {
    return this.ProveedorCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setProveedorCambio(Proveedores: Proveedor[]) {
    this.ProveedorCambio.next(Proveedores);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
