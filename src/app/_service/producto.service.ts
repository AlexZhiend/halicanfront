import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Producto } from '../_model/producto';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductoService extends GenericService<Producto> {

  private ProductoCambio = new Subject<Producto[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/producto`);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  //get Subjects
  getProductoCambio() {
    return this.ProductoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setProductoCambio(Productoes: Producto[]) {
    this.ProductoCambio.next(Productoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  buscarnombrecod(s:string){
    return this.http.get<Producto[]>(`${this.url}/buscarnombrecod/${s}`);
  }

  buscarporsubcategoria(subcategoria:string){
    return this.http.get<Producto[]>(`${this.url}/buscarsubcategoria/${subcategoria}`);
  }

}
