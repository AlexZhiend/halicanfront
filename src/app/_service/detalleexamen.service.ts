import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DetalleExamen } from '../_model/detalleexamen';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class DetalleexamenService extends GenericService<DetalleExamen> {

  private DetalleExamenCambio = new Subject<DetalleExamen[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/detalleexamen`);
  }

  //get Subjects
  getDetalleExamenCambio() {
    return this.DetalleExamenCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setDetalleExamenCambio(DetalleExamenes: DetalleExamen[]) {
    this.DetalleExamenCambio.next(DetalleExamenes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
