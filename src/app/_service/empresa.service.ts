import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Empresa } from '../_model/empresa';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService extends GenericService<Empresa> {

  private EmpresaCambio = new Subject<Empresa[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/empresa`);
  }

  //get Subjects
  getEmpresaCambio() {
    return this.EmpresaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setEmpresaCambio(Empresaes: Empresa[]) {
    this.EmpresaCambio.next(Empresaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
