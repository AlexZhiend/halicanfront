import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Rol } from '../_model/rol';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolService extends GenericService<Rol> {

  private RolCambio = new Subject<Rol[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/rol`);
  }

  //get Subjects
  getRolCambio() {
    return this.RolCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setRolCambio(Roles: Rol[]) {
    this.RolCambio.next(Roles);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
