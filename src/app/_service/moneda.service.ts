import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Moneda } from '../_model/moneda';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MonedaService extends GenericService<Moneda> {

  private MonedaCambio = new Subject<Moneda[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/moneda`);
  }

  //get Subjects
  getMonedaCambio() {
    return this.MonedaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setMonedaCambio(Monedaes: Moneda[]) {
    this.MonedaCambio.next(Monedaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
