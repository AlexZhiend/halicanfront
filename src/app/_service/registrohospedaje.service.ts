import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RegistroDeHospedaje } from '../_model/registrodehospedaje';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistrohospedajeService extends GenericService<RegistroDeHospedaje> {

  private RegistroDeHospedajeCambio = new Subject<RegistroDeHospedaje[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/registrodehospedaje`);
  }

  //get Subjects
  getRegistroDeHospedajeCambio() {
    return this.RegistroDeHospedajeCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setRegistroDeHospedajeCambio(RegistroDeHospedajees: RegistroDeHospedaje[]) {
    this.RegistroDeHospedajeCambio.next(RegistroDeHospedajees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }
}
