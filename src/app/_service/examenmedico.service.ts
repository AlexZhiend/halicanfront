import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ExamenMedico } from '../_model/examenmedico';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExamenmedicoService extends GenericService<ExamenMedico> {

  private ExamenMedicoCambio = new Subject<ExamenMedico[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/examenmedico`);
  }

  //get Subjects
  getExamenMedicoCambio() {
    return this.ExamenMedicoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setExamenMedicoCambio(ExamenMedicoes: ExamenMedico[]) {
    this.ExamenMedicoCambio.next(ExamenMedicoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
