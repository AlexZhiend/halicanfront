import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Comprobante } from '../_model/comprobante';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ComprobanteService extends GenericService<Comprobante> {

  private ComprobanteCambio = new Subject<Comprobante[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/comprobante`);
  }

  //get Subjects
  getComprobanteCambio() {
    return this.ComprobanteCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setComprobanteCambio(Comprobantees: Comprobante[]) {
    this.ComprobanteCambio.next(Comprobantees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  generarTicket(correlativo:number,idserie:number){
    return this.http.get(`${this.url}/ticketpdf/${correlativo}/${idserie}`,{
      responseType:'blob'
    })
  }

  comprobanteporfechas(fechainicio:string,fechafin:string){
    return this.http.get<Comprobante[]>(`${this.url}/comprobantebyfechas/${fechainicio}/${fechafin}`)
  }

  reportecajaporfecha(fechainicio:string,fechafin:string){
    return this.http.get(`${this.url}/reportecaja/${fechainicio}/${fechafin}`,{
      responseType:'blob'
    })
  }

}
