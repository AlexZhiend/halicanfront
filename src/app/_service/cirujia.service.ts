import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Cirujia } from '../_model/cirujia';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CirujiaService extends GenericService<Cirujia>{

  private CirujiaCambio = new Subject<Cirujia[]>();
  private mensajeCambio = new Subject<string>();

  private cirujiabuscado = new Subject<Cirujia>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/cirujia`);
  }

  getCirujiaadd(){
    return this.cirujiabuscado.asObservable()
  }

  setCirujiaadd(cirujiaseleccionado: Cirujia) {
    this.cirujiabuscado.next(cirujiaseleccionado);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  //get Subjects
  getCirujiaCambio() {
    return this.CirujiaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setCirujiaCambio(Cirujiaes: Cirujia[]) {
    this.CirujiaCambio.next(Cirujiaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
