import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PersonalVeterinario } from '../_model/personalveterinario';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonalveterinarioService extends GenericService<PersonalVeterinario> {

  private PersonalVeterinarioCambio = new Subject<PersonalVeterinario[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/personalveterinario`);
  }

  //get Subjects
  getPersonalVeterinarioCambio() {
    return this.PersonalVeterinarioCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setPersonalVeterinarioCambio(PersonalVeterinarioes: PersonalVeterinario[]) {
    this.PersonalVeterinarioCambio.next(PersonalVeterinarioes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
