import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RegistroDeBanio } from '../_model/registrodebanio';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistrobanioService extends GenericService<RegistroDeBanio> {

  private RegistroDeBanioCambio = new Subject<RegistroDeBanio[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/registrobanio`);
  }

  //get Subjects
  getRegistroDeBanioCambio() {
    return this.RegistroDeBanioCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setRegistroDeBanioCambio(RegistroDeBanioes: RegistroDeBanio[]) {
    this.RegistroDeBanioCambio.next(RegistroDeBanioes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

}
