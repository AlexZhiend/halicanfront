import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RegistroDesparasitacion } from '../_model/registrodesparasitacion';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistrodesparasitacionService extends GenericService<RegistroDesparasitacion> {

  private RegistroDesparasitacionCambio = new Subject<RegistroDesparasitacion[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/registrodesparasitacion`);
  }

  //get Subjects
  getRegistroDesparasitacionCambio() {
    return this.RegistroDesparasitacionCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setRegistroDesparasitacionCambio(RegistroDesparasitaciones: RegistroDesparasitacion[]) {
    this.RegistroDesparasitacionCambio.next(RegistroDesparasitaciones);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }
}
