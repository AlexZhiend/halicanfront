import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Unidad } from '../_model/unidad';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UnidadService extends GenericService<Unidad> {

  private UnidadCambio = new Subject<Unidad[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/unidad`);
  }

  //get Subjects
  getUnidadCambio() {
    return this.UnidadCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setUnidadCambio(Unidades: Unidad[]) {
    this.UnidadCambio.next(Unidades);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
