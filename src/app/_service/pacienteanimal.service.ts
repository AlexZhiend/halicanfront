import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { PacienteAnimal } from '../_model/pacienteanimal';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PacienteanimalService extends GenericService<PacienteAnimal> {

  private PacienteAnimalCambio = new Subject<PacienteAnimal[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/pacienteanimal`);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  //get Subjects
  getPacienteAnimalCambio() {
    return this.PacienteAnimalCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setPacienteAnimalCambio(PacienteAnimales: PacienteAnimal[]) {
    this.PacienteAnimalCambio.next(PacienteAnimales);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  buscarPorCliente(s:string){
    return this.http.get<PacienteAnimal[]>(`${this.url}/buscarporcliente/${s}`);
  }

  buscarPorNombre(s:string){
    return this.http.get<PacienteAnimal[]>(`${this.url}/buscarpornombre/${s}`);
  }

  buscarUltimaHistoria(){
    return this.http.get<PacienteAnimal>(`${this.url}/buscarultimahistoria`);
  }
}
