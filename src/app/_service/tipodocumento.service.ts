import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoDocumento } from '../_model/tipodocumento';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipodocumentoService extends GenericService<TipoDocumento> {

  private TipoDocumentoCambio = new Subject<TipoDocumento[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tipodocumento`);
  }

  //get Subjects
  getTipoDocumentoCambio() {
    return this.TipoDocumentoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTipoDocumentoCambio(TipoDocumentoes: TipoDocumento[]) {
    this.TipoDocumentoCambio.next(TipoDocumentoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
