import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { Serie } from '../_model/serie';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SerieService extends GenericService<Serie> {

  private SerieCambio = new Subject<Serie[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/serie`);
  }

  //get Subjects
  getSerieCambio() {
    return this.SerieCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setSerieCambio(Seriees: Serie[]) {
    this.SerieCambio.next(Seriees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  buscarcorrelativo(s:string,i:number){
    return this.http.get<Serie>(`${this.url}/buscarcorrelativo/${s}/${i}`);
  }

  buscarserietipo(s:number){
    return this.http.get<Serie[]>(`${this.url}/buscarserietipo/${s}`);
  }

}
