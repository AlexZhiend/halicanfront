import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { TipoCorte } from '../_model/tipocorte';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipocorteService extends GenericService<TipoCorte> {

  private TipoCorteCambio = new Subject<TipoCorte[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tipocorte`);
  }

  //get Subjects
  getTipoCorteCambio() {
    return this.TipoCorteCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTipoCorteCambio(TipoCortees: TipoCorte[]) {
    this.TipoCorteCambio.next(TipoCortees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
