import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GenericService } from './generic.service';
import { Hospitalizacion } from '../_model/hospitalizacion';

@Injectable({
  providedIn: 'root'
})
export class HospitalizacionService extends GenericService<Hospitalizacion> {

  private HospitalizacionCambio = new Subject<Hospitalizacion[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/hospitalizacion`);
  }

  //get Subjects
  getHospitalizacionCambio() {
    return this.HospitalizacionCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setHospitalizacionCambio(Hospitalizaciones: Hospitalizacion[]) {
    this.HospitalizacionCambio.next(Hospitalizaciones);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }


  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }
}
