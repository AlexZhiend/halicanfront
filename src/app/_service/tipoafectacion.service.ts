import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoAfectacion } from '../_model/tipoafectacion';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoafectacionService extends GenericService<TipoAfectacion> {

  private TipoAfectacionCambio = new Subject<TipoAfectacion[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tipoafectacion`);
  }

  //get Subjects
  getTipoAfectacionCambio() {
    return this.TipoAfectacionCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTipoAfectacionCambio(TipoAfectaciones: TipoAfectacion[]) {
    this.TipoAfectacionCambio.next(TipoAfectaciones);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
