import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { EstadoInterno } from '../_model/estadointerno';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstadointernoService extends GenericService<EstadoInterno> {

  private EstadoInternoCambio = new Subject<EstadoInterno[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/estadointerno`);
  }

  //get Subjects
  getEstadoInternoCambio() {
    return this.EstadoInternoCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setEstadoInternoCambio(EstadoInternoes: EstadoInterno[]) {
    this.EstadoInternoCambio.next(EstadoInternoes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
