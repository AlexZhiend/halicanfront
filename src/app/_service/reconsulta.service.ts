import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Reconsulta } from '../_model/reconsulta';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class ReconsultaService extends GenericService<Reconsulta> {

  private ReconsultaCambio = new Subject<Reconsulta[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/reconsulta`);
  }

  //get Subjects
  getReconsultaCambio() {
    return this.ReconsultaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setReconsultaCambio(Reconsultaes: Reconsulta[]) {
    this.ReconsultaCambio.next(Reconsultaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
