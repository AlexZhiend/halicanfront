import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RegistroVacunas } from '../_model/registrovacunas';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistrovacunasService extends GenericService<RegistroVacunas> {

  private RegistroVacunasCambio = new Subject<RegistroVacunas[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/registrovacunas`);
  }

  //get Subjects
  getRegistroVacunasCambio() {
    return this.RegistroVacunasCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setRegistroVacunasCambio(RegistroVacunases: RegistroVacunas[]) {
    this.RegistroVacunasCambio.next(RegistroVacunases);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

}
