import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoBanio } from '../_model/tipobanio';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class TipobanioService extends GenericService<TipoBanio> {

  private TipoBanioCambio = new Subject<TipoBanio[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tipobanio`);
  }

  //get Subjects
  getTipoBanioCambio() {
    return this.TipoBanioCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTipoBanioCambio(TipoBanioes: TipoBanio[]) {
    this.TipoBanioCambio.next(TipoBanioes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
