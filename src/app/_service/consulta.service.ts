import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Consulta } from '../_model/consulta';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService extends GenericService<Consulta> {

  private ConsultaCambio = new Subject<Consulta[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/consulta`);
  }

  //get Subjects
  getConsultaCambio() {
    return this.ConsultaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setConsultaCambio(Consultaes: Consulta[]) {
    this.ConsultaCambio.next(Consultaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

}
