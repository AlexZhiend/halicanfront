import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoComprobante } from '../_model/tipocomprobante';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipocomprobanteService extends GenericService<TipoComprobante> {

  private TipoComprobanteCambio = new Subject<TipoComprobante[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/tipocomprobante`);
  }

  buscarcodigosunat(s:string){
    return this.http.get<TipoComprobante>(`${this.url}/buscarporcodigo/${s}`);
  }

  //get Subjects
  getTipoComprobanteCambio() {
    return this.TipoComprobanteCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setTipoComprobanteCambio(TipoComprobantees: TipoComprobante[]) {
    this.TipoComprobanteCambio.next(TipoComprobantees);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
