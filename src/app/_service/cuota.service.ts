import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Cuota } from '../_model/cuota';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuotaService extends GenericService<Cuota> {

  private CuotaCambio = new Subject<Cuota[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected override http: HttpClient) {
    super(
      http,
      `${environment.HOST}/cuota`);
  }

  //get Subjects
  getCuotaCambio() {
    return this.CuotaCambio.asObservable();
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  //set Subjects
  setCuotaCambio(Cuotaes: Cuota[]) {
    this.CuotaCambio.next(Cuotaes);
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
