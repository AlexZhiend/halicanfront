import { Component, OnInit, Inject } from '@angular/core';
import { TipodocumentoService } from '../../../_service/tipodocumento.service';
import { TipoDocumento } from '../../../_model/tipodocumento';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, switchMap } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Cliente } from '../../../_model/cliente';
import { map } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ClienteService } from '../../../_service/cliente.service';

@Component({
  selector: 'app-cliente-dialog',
  templateUrl: './cliente-dialog.component.html',
  styleUrls: ['./cliente-dialog.component.css']
})
export class ClienteDialogComponent implements OnInit {

  tipodocumentos: TipoDocumento[]=[];
  maxFecha: Date = new Date();

  cliente: Cliente;

  form:FormGroup;
  MycontrolTipo= new FormControl();

  filteredOptions: Observable<TipoDocumento[]>;
  tipodocumento= new TipoDocumento();

  tipodocumentoseleccionado:number;
  tipoDocumentoFiltrados$: Observable<TipoDocumento[]>;

  constructor(private tipodocumentoService: TipodocumentoService,
              public dialogRef: MatDialogRef<ClienteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Cliente,
              private clienteservice: ClienteService) {
                this.form=new FormGroup({
                  'tipos': this.MycontrolTipo,
                  'id':new FormControl(),
                  'dni':new FormControl(),
                  'nombresyapellidos':new FormControl(),
                  'razonsocial':new FormControl(),
                  'departamento':new FormControl(),
                  'distrito':new FormControl(),
                  'provincia':new FormControl(),
                  'direccion':new FormControl(),
                  'telefono':new FormControl(),

                });
   }

  ngOnInit(): void {
    this.listarTipoDocumento();
    this.cliente = { ...this.data };

    if(this.cliente.idcliente!= null){
      this.form.patchValue({
        id:this.data.idcliente,
        dni:this.data.dni,
        razonsocial:this.data.razonsocial,
        nombresyapellidos:this.data.nombresyapellidos,
        direccion:this.data.direccion,
        telefono:this.data.telefono,
        departamento:this.data.departamento,
        provincia:this.data.provincia,
        distrito:this.data.distrito,
      });
      this.MycontrolTipo.setValue(this.data.tipodocumento);
      this.tipodocumentoseleccionado=this.data.tipodocumento.idtipodocumento;
    }
    this.tipoDocumentoFiltrados$ = this.MycontrolTipo.valueChanges.pipe(
      map(val => this.filtrarTipodocumento(val)));
  }

  listarTipoDocumento() {
    this.tipodocumentoService.listar().subscribe(data => {
      this.tipodocumentos=data;
    });
  }

  mostrarTipodocumento(val: any) {
    return val ? `${val.descripcion}` : val;
  }

  filtrarTipodocumento(val: any) {
    if (val != null && val.idtipodocumento > 0) {
      return this.tipodocumentos.filter(option =>
        option.descripcion.toLowerCase().includes(val.descripcion.toLowerCase()) ||
        option.idtipodocumento.toString().includes(val.idtipodocumento));
    } else {
      return this.tipodocumentos.filter(option =>
        option.descripcion.toLowerCase().includes(val?.toLowerCase()) ||
        option.idtipodocumento.toString().includes(val));
    }
  }

  aceptar(){
    let cliente1 = new Cliente();
    cliente1.idcliente=this.form.value['id']
    cliente1.dni = this.form.value['dni'];
    cliente1.nombresyapellidos = this.form.value['nombresyapellidos'];
    cliente1.razonsocial = this.form.value['razonsocial'];
    cliente1.direccion = this.form.value['direccion'];
    cliente1.telefono = this.form.value['telefono'];
    cliente1.departamento = this.form.value['departamento'];
    cliente1.distrito = this.form.value['distrito'];
    cliente1.provincia = this.form.value['provincia'];
    cliente1.tipodocumento = this.form.value['tipos'];
    this.cliente=cliente1;

    if (this.form.valid == true) {
      if (this.cliente != null && this.cliente.idcliente > 0 ) {
        //MODIFICAR
        this.clienteservice.modificar(this.cliente).pipe(switchMap(() => {
          return this.clienteservice.listar()
        }))
        .subscribe(data => {
          this.clienteservice.setClienteCambio(data);
          this.clienteservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.clienteservice.registrar(this.cliente).subscribe(() => {
          this.clienteservice.listar().subscribe(data => {
            this.clienteservice.setClienteCambio(data);
            this.clienteservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.clienteservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}
