import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Empresa } from '../../_model/empresa';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { EmpresaService } from '../../_service/empresa.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmpresaDialogoComponent } from './empresa-dialogo/empresa-dialogo.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {


  displayedColumns = ['idempresa','ruc','nombrecomercial','razonsocial', 'acciones'];
  dataSource: MatTableDataSource<Empresa>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private empresaService: EmpresaService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.empresaService.getEmpresaCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.empresaService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.empresaService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Empresa[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(empresa?: Empresa) {
    this.dialog.open(EmpresaDialogoComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: empresa
    });
  }

  eliminar(empresa: Empresa) {
    this.empresaService.eliminar(empresa.idempresa).pipe(switchMap(() => {
      return this.empresaService.listar();
    }))
      .subscribe(data => {
        this.empresaService.setMensajeCambio("SE ELIMINO");
        this.empresaService.setEmpresaCambio(data);
      });
  }
}
