import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Empresa } from '../../../_model/empresa';
import { CategoriaDialogoComponent } from '../../categoria/categoria-dialogo/categoria-dialogo.component';
import { EmpresaService } from '../../../_service/empresa.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-empresa-dialogo',
  templateUrl: './empresa-dialogo.component.html',
  styleUrls: ['./empresa-dialogo.component.css']
})
export class EmpresaDialogoComponent implements OnInit {

  empresa: Empresa;

  constructor(
    private dialogRef: MatDialogRef<CategoriaDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Empresa,
    private empresaService : EmpresaService
  ) { }

  ngOnInit(): void {
    this.empresa = { ...this.data };
  }

  operar() {
    if (this.empresa.nombrecomercial!=null && this.empresa.ruc!=null && this.empresa.razonsocial!=null) {
      if (this.empresa != null && this.empresa.idempresa > 0 ) {
        //MODIFICAR
        this.empresaService.modificar(this.empresa).pipe(switchMap(() => {
          return this.empresaService.listar();
        }))
        .subscribe(data => {
          this.empresaService.setEmpresaCambio(data);
          this.empresaService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.empresaService.registrar(this.empresa).subscribe(() => {
          this.empresaService.listar().subscribe(data => {
            this.empresaService.setEmpresaCambio(data);
            this.empresaService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.empresaService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }



  }

  cerrar() {
    this.dialogRef.close();
  }
}
