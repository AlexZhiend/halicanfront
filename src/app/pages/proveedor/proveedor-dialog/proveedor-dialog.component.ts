import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaDialogoComponent } from '../../categoria/categoria-dialogo/categoria-dialogo.component';
import { Proveedor } from '../../../_model/proveedor';
import { ProveedorService } from '../../../_service/proveedor.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-proveedor-dialog',
  templateUrl: './proveedor-dialog.component.html',
  styleUrls: ['./proveedor-dialog.component.css']
})
export class ProveedorDialogComponent implements OnInit {

  proveedor: Proveedor;

  constructor(
    private dialogRef: MatDialogRef<CategoriaDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Proveedor,
    private proveedorService : ProveedorService
  ) { }

  ngOnInit(): void {
    this.proveedor = { ...this.data };
  }

  operar() {
    if (this.proveedor.nombre!='' && this.proveedor.ruc!='' &&this.proveedor.telefono!= '') {
      if (this.proveedor != null && this.proveedor.idproveedor > 0 ) {
        //MODIFICAR
        this.proveedorService.modificar(this.proveedor).pipe(switchMap(() => {
          return this.proveedorService.listar();
        }))
        .subscribe(data => {
          this.proveedorService.setProveedorCambio(data);
          this.proveedorService.setMensajeCambio("SE MODIFICÓ");
        });

      } else {
        //REGISTRAR
        this.proveedorService.registrar(this.proveedor).subscribe(() => {
          this.proveedorService.listar().subscribe(data => {
            this.proveedorService.setProveedorCambio(data);
            this.proveedorService.setMensajeCambio("SE REGISTRÓ");
          });
        });
      }
      this.cerrar();
    }else{
      this.proveedorService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar() {
    this.dialogRef.close();
  }
}
