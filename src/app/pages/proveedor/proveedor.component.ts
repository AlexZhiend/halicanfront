import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Proveedor } from '../../_model/proveedor';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProveedorService } from '../../_service/proveedor.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProveedorDialogComponent } from './proveedor-dialog/proveedor-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {

  displayedColumns = ['ruc','nombre','representante','telefono','acciones'];
  dataSource: MatTableDataSource<Proveedor>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private proveedorService: ProveedorService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.proveedorService.getProveedorCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.proveedorService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.proveedorService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Proveedor[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(proveedor?: Proveedor) {
    this.dialog.open(ProveedorDialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: proveedor
    });
  }

  eliminar(proveedor: Proveedor) {
    this.proveedorService.eliminar(proveedor.idproveedor).pipe(switchMap(() => {
      return this.proveedorService.listar();
    }))
      .subscribe(data => {
        this.proveedorService.setMensajeCambio("SE ELIMINO");
        this.proveedorService.setProveedorCambio(data);
      });
  }

}
