import { Component, OnInit, ViewChild } from '@angular/core';
import { Consulta } from '../../_model/consulta';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ConsultaService } from '../../_service/consulta.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { ConsultadialogComponent } from './consultadialog/consultadialog.component';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  dataSource: MatTableDataSource<Consulta>;
  displayedColumns: string[] = ['idconsulta','nombre','historia','personalveterinario','fecha','acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  condition1=1;
  condition2=2;
  condition3=3;

  constructor(
    private consultaService: ConsultaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.consultaService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.consultaService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.consultaService.getConsultaCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.consultaService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.consultaService.eliminar(id).pipe(switchMap( ()=> {
      return this.consultaService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.consultaService.setConsultaCambio(data);
      this.consultaService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Consulta[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.consultaService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(consulta?: Consulta) {
    this.dialog.open(ConsultadialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: consulta
    });
  }


}
