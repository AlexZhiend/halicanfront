import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Consulta } from '../../../_model/consulta';
import { PersonalVeterinario } from '../../../_model/personalveterinario';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable, switchMap } from 'rxjs';
import { PersonalveterinarioService } from '../../../_service/personalveterinario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConsultaService } from '../../../_service/consulta.service';
import { map } from 'rxjs/operators';
import { MatAccordion } from '@angular/material/expansion';
import { MatTableDataSource } from '@angular/material/table';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { ConstantesFisiologicas } from '../../../_model/constantesfisiologicas';


@Component({
  selector: 'app-consultadialog',
  templateUrl: './consultadialog.component.html',
  styleUrls: ['./consultadialog.component.css']
})
export class ConsultadialogComponent implements OnInit {

  panelOpenState = false;
  @ViewChild(MatAccordion) accordion: MatAccordion;

  maxFecha: Date = new Date();
  consulta: Consulta;
  personalveterinarios: PersonalVeterinario[]=[];
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];

  form:FormGroup;
  form2:FormGroup;
  MycontrolTipo = new FormControl();

  fechaingreso:Date=new Date();
  fecha1=new FormControl(this.maxFecha);
  fecha2= new Date();
  fecha3=new FormControl(this.maxFecha);
  fecha4= new Date();
  personalveterinarioseleccionado:PersonalVeterinario;

  personalveterinarioFiltrados$: Observable<PersonalVeterinario[]>;
  personalveterinarios$: Observable<PersonalVeterinario[]>;


  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];

  dataSource2: MatTableDataSource<ConstantesFisiologicas>;
  displayedColumns2: string[] = ['fecha', 'consciente', 'hidratacion','mucosas','peso',
  'frecuenciacardiaca','frecuenciarespiratoria','pulso','tllenadocapilar',
  'temperaturas','descripcionexamen','acciones1'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  nombreodni: string='';
  nombreanimal: string='';

  concientes=['SI','NO'];
  hidrataciones=['5%','7%','10%','12%'];

  constante= new ConstantesFisiologicas();
  constantes:ConstantesFisiologicas[]=[];

  constructor(private personalveterinarioService:PersonalveterinarioService,
              public dialogRef: MatDialogRef<ConsultadialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Consulta,
              private pacienteanimalservice:PacienteanimalService,
              private consultaservice: ConsultaService,
              private snakbar:MatSnackBar) {
                this.form=new FormGroup({
                  'personalveterinario': this.MycontrolTipo,
                  'id':new FormControl(),
                  'motivo':new FormControl(),
                  'signossintomas':new FormControl(),
                  'receta':new FormControl(),
                  'inicio':new FormControl(),
                  'paciente':new FormControl()
                });
                this.form2=new FormGroup({
                  'fecha':this.fecha3,
                  'consciente':new FormControl(),
                  'hidratacion':new FormControl(),
                  'mucosas':new FormControl(),
                  'peso':new FormControl(),
                  'fc':new FormControl(),
                  'fr':new FormControl(),
                  'fp':new FormControl(),
                  'tllc':new FormControl(),
                  'temperatura':new FormControl(),
                  'descripcionexamen':new FormControl(),
                })
   }

  ngOnInit(): void {
    this.listarPersonalveterinario();

    this.consulta = { ...this.data };

    if(this.consulta.idconsulta!= null){
      this.form.patchValue({
        id:this.data.idconsulta,
        motivo:this.data.motivo,
        signossintomas:this.data.signossintomas,
        inicio:this.data.inicio,
        paciente:this.data.pacienteanimal.nombre,
        receta:this.data.receta
        });
      this.MycontrolTipo.setValue(this.data.personalveterinario);
      this.personalveterinarioseleccionado=this.data.personalveterinario;
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fecha);
      this.constantes=this.data.constantesfisiologicas;
      this.dataSource2= new MatTableDataSource(this.constantes);
      console.log(this.data)
    }


    this.personalveterinarioFiltrados$ = this.MycontrolTipo.valueChanges.pipe(
      map(val => this.filtrarPersonalveterinario(val)));
  }

  listarPersonalveterinario(){
    this.personalveterinarioService.listar().subscribe(data =>{
      this.personalveterinarios=data;
    });
  }

  mostrarPersonalveterinario(val: any) {
    return val ? `${val.nombrespersonal}` : val;
  }

  filtrarPersonalveterinario(val: any) {
    if (val != null && val.idpersonal > 0) {
      return this.personalveterinarios.filter(option =>
        option.nombrespersonal.toLowerCase().includes(val.nombrespersonal.toLowerCase()) )

    } else {
      return this.personalveterinarios.filter(option =>
        option.nombrespersonal.toLowerCase().includes(val?.toLowerCase()) )
    }
  }

  aceptar(){
    let consulta1 = new Consulta();
    consulta1.idconsulta=this.form.value['id']
    consulta1.inicio = this.form.value['inicio'];
    consulta1.motivo = this.form.value['motivo'];
    consulta1.signossintomas = this.form.value['signossintomas'];
    consulta1.receta=this.form.value['receta'];

    // console.log(this.fecha1.value)
    // var tzoffset = (this.fecha1.value).getTimezoneOffset()*60000 ;
    // var localisotime=(new Date(Date.now()-tzoffset)).toISOString();
    // consulta1.fecha = localisotime;

    this.fecha2 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha2).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha2.getTime()-tzoffset));
    consulta1.fecha = localisotime.toISOString();

    let pac = new PacienteAnimal();
    let pers=new PersonalVeterinario();
    pers=this.personalveterinarioseleccionado;
    pac=this.pacienteseleccionado;
    consulta1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;
    consulta1.personalveterinario = this.form.value['personalveterinario'];
    consulta1.constantesfisiologicas=this.constantes;

    this.consulta=consulta1;
    console.log(this.consulta);

    if (this.form.valid == true) {
      if (this.consulta != null && this.consulta.idconsulta > 0 ) {
        //MODIFICAR
        this.consultaservice.modificar(this.consulta).pipe(switchMap(() => {
          return this.consultaservice.listar()
        }))
        .subscribe(data => {
          this.consultaservice.setConsultaCambio(data);
          console.log(data)
          this.consultaservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.consultaservice.registrar(this.consulta).subscribe(() => {
          this.consultaservice.listar().subscribe(data => {
            this.consultaservice.setConsultaCambio(data);
            this.consultaservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.consultaservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
    this.panelOpenState=false;
  }


  eliminar(row:ConstantesFisiologicas){
    console.log(row)
  }

  agregarconstante(){


    let constante1= new ConstantesFisiologicas();

    this.fecha4 = new Date(this.fecha3.value);
    var tzoffset = (this.fecha4).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha4.getTime()-tzoffset));

    constante1.fecha= localisotime.toISOString();
    constante1.consciente=this.form2.value['consciente'];
    constante1.descripcionexamen=this.form2.value['descripcionexamen'];
    constante1.frecuenciacardiaca=this.form2.value['fc'];
    constante1.frecuenciarespiratoria=this.form2.value['fr'];
    constante1.hidratacion=this.form2.value['hidratacion'];
    constante1.idconstantes=this.form2.value['id'];
    constante1.mucosas=this.form2.value['mucosas'];
    constante1.peso=this.form2.value['peso'];
    constante1.pulso=this.form2.value['fp'];
    constante1.temperaturas=this.form2.value['temperatura'];
    constante1.tllenadocapilar=this.form2.value['tllc'];

    this.constantes.push(constante1);
    this.dataSource2= new MatTableDataSource(this.constantes);
    this.form2.reset();
  }

  limpiar(){
    this.form2.reset();
  }

  eliminarconst(e:ConstantesFisiologicas){
    var indice=this.constantes.indexOf(e);
    this.constantes.splice(indice, 1);
    this.dataSource2 = new MatTableDataSource(this.constantes);
  }

}
