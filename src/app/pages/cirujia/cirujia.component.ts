import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Cirujia } from '../../_model/cirujia';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CirujiaService } from '../../_service/cirujia.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { CirujiadialogComponent } from './cirujiadialog/cirujiadialog.component';

@Component({
  selector: 'app-cirujia',
  templateUrl: './cirujia.component.html',
  styleUrls: ['./cirujia.component.css']
})
export class CirujiaComponent implements OnInit {


  dataSource: MatTableDataSource<Cirujia>;
  displayedColumns: string[] = ['idcirujia', 'paciente',
  'propietario','telefono','intervencion', 'fecha','acciones'];
  cantidad: number;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private cirujiaService: CirujiaService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.cirujiaService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.cirujiaService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.cirujiaService.getCirujiaCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.cirujiaService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.cirujiaService.eliminar(id).pipe(switchMap( ()=> {
      return this.cirujiaService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.cirujiaService.setCirujiaCambio(data);
      this.cirujiaService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Cirujia[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.cirujiaService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(cirujia?: Cirujia) {
    this.dialog.open(CirujiadialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: cirujia
    });
  }

}
