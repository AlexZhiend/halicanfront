import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Producto } from '../../../_model/producto';
import { Cirujia } from '../../../_model/cirujia';
import { FormControl, FormGroup } from '@angular/forms';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CirujiaService } from '../../../_service/cirujia.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { ProductoService } from '../../../_service/producto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-cirujiadialog',
  templateUrl: './cirujiadialog.component.html',
  styleUrls: ['./cirujiadialog.component.css']
})
export class CirujiadialogComponent implements OnInit {


  dataSource: MatTableDataSource<PacienteAnimal>;
  dataSource2: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  displayedColumns2: string[] = ['codigo','intervencion','acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  intervencionselec= new Producto();
  vacunas:Producto[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha3= new Date();

  cirujia= new Cirujia();


  constructor(private cirujiaService:CirujiaService,
    public dialogRef: MatDialogRef<CirujiadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cirujia,
    private pacienteanimalservice:PacienteanimalService,
    private productoService:ProductoService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'hora': new FormControl(),
        'fecha': this.fecha1,
        'intervencion': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {
    this.listarvacunas();

    this.cirujia = { ...this.data };

    if(this.cirujia.idcirujia!= null){
      this.form.patchValue({
        id:this.data.idcirujia,
        fecha:this.data.fecha,
        hora:this.data.hora,
        intervencion:this.data.intervencion,
        paciente:this.data.pacienteanimal.nombre,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fecha)
    }

  }

  listarvacunas(){
    this.productoService.buscarporsubcategoria('INTERVENCIONES').subscribe(data=>{
      console.log(data)
      this.dataSource2= new MatTableDataSource(data);
    })
  }

  aceptar(){
    let cirujia1 = new Cirujia();
    cirujia1.idcirujia=this.form.value['id']
    cirujia1.observaciones = this.form.value['observaciones'];
    cirujia1.intervencion = this.form.value['intervencion'];

    cirujia1.hora = this.form.value['hora'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    cirujia1.fecha = localisotime;

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    cirujia1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;

    this.cirujia=cirujia1;
    console.log(this.cirujia)
    if (this.form.valid == true) {
      if (this.cirujia != null && this.cirujia.idcirujia > 0 ) {
        //MODIFICAR
        this.cirujiaService.modificar(this.cirujia).pipe(switchMap(() => {
          return this.cirujiaService.listar()
        }))
        .subscribe(data => {
          this.cirujiaService.setCirujiaCambio(data);
          console.log(data)
          this.cirujiaService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.cirujiaService.registrar(this.cirujia).subscribe(() => {
          this.cirujiaService.listar().subscribe(data => {
            this.cirujiaService.setCirujiaCambio(data);
            this.cirujiaService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.cirujiaService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }

  agregarintervencion(row:Producto){
    this.intervencionselec=row;
    this.form.patchValue({
      intervencion:this.intervencionselec.nombre,
    })
    this.snakbar.open('La intervención se seleccionó correctamente','Aviso',{duration:1000});
  }

}
