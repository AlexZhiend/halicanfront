import { Component, OnInit, Inject, HostBinding } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { observable, switchMap } from 'rxjs';
import { CategoriaProducto } from '../../../_model/categoriaproducto';
import { CategoriaProductoService } from '../../../_service/categoria-producto.service';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-categoria-dialogo',
  templateUrl: './categoria-dialogo.component.html',
  styleUrls: ['./categoria-dialogo.component.css']
})
export class CategoriaDialogoComponent implements OnInit {

  categoriaproducto: CategoriaProducto;

  constructor(
    private dialogRef: MatDialogRef<CategoriaDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: CategoriaProducto,
    private categoriaproductoService : CategoriaProductoService,
  ) { }

  ngOnInit(): void {
    this.categoriaproducto = { ...this.data };
  }

  operar() {
    if (this.categoriaproducto.nombre!=null) {
      if (this.categoriaproducto != null && this.categoriaproducto.idcategoriaproducto > 0 ) {
        //MODIFICAR
        this.categoriaproductoService.modificar(this.categoriaproducto).pipe(switchMap(() => {
          return this.categoriaproductoService.listar();
        }))
        .subscribe(data => {
          this.categoriaproductoService.setCategoriaProductoCambio(data);
          this.categoriaproductoService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.categoriaproductoService.registrar(this.categoriaproducto).subscribe(() => {
          this.categoriaproductoService.listar().subscribe(data => {
            this.categoriaproductoService.setCategoriaProductoCambio(data);
            this.categoriaproductoService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.categoriaproductoService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }



  }

  cerrar() {
    this.dialogRef.close();
  }

}
