import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CategoriaProducto } from '../../_model/categoriaproducto';
import { CategoriaProductoService } from '../../_service/categoria-producto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';
import { CategoriaDialogoComponent } from '../categoria/categoria-dialogo/categoria-dialogo.component';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  displayedColumns = ['idcategoriaproducto', 'nombre', 'acciones'];
  dataSource: MatTableDataSource<CategoriaProducto>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private categoriaproductoService: CategoriaProductoService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.categoriaproductoService.getCategoriaProductoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.categoriaproductoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.categoriaproductoService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: CategoriaProducto[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(categoriaproducto?: CategoriaProducto) {
    this.dialog.open(CategoriaDialogoComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: categoriaproducto
    });
  }

  eliminar(categoriaproducto: CategoriaProducto) {
    this.categoriaproductoService.eliminar(categoriaproducto.idcategoriaproducto).pipe(switchMap(() => {
      return this.categoriaproductoService.listar();
    }))
      .subscribe(data => {
        this.categoriaproductoService.setMensajeCambio("SE ELIMINO");
        this.categoriaproductoService.setCategoriaProductoCambio(data);
      });
  }

}
