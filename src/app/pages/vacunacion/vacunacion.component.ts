import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductoService } from '../../_service/producto.service';
import { RegistrovacunasService } from '../../_service/registrovacunas.service';
import { RegistroVacunas } from '../../_model/registrovacunas';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs';
import { VacunaciondialogComponent } from './vacunaciondialog/vacunaciondialog.component';

@Component({
  selector: 'app-vacunacion',
  templateUrl: './vacunacion.component.html',
  styleUrls: ['./vacunacion.component.css']
})
export class VacunacionComponent implements OnInit {


  dataSource: MatTableDataSource<RegistroVacunas>;
  displayedColumns: string[] = ['idregistrovacunas', 'paciente',
  'propietario','telefono','vacuna', 'fechaaplicacion','fechasiguiente', 'acciones'];
  cantidad: number;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private registrovacunasService: RegistrovacunasService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.registrovacunasService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.registrovacunasService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.registrovacunasService.getRegistroVacunasCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.registrovacunasService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.registrovacunasService.eliminar(id).pipe(switchMap( ()=> {
      return this.registrovacunasService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.registrovacunasService.setRegistroVacunasCambio(data);
      this.registrovacunasService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: RegistroVacunas[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.registrovacunasService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(registrovacunas?: RegistroVacunas) {
    this.dialog.open(VacunaciondialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: registrovacunas
    });
  }
}
