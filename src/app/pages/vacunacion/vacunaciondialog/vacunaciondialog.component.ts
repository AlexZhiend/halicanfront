import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VacunacionComponent } from '../vacunacion.component';
import { RegistrovacunasService } from '../../../_service/registrovacunas.service';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl } from '@angular/forms';
import { ProductoService } from '../../../_service/producto.service';
import { Producto } from 'src/app/_model/producto';
import { RegistroVacunas } from '../../../_model/registrovacunas';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-vacunaciondialog',
  templateUrl: './vacunaciondialog.component.html',
  styleUrls: ['./vacunaciondialog.component.css']
})
export class VacunaciondialogComponent implements OnInit {

  dataSource: MatTableDataSource<PacienteAnimal>;
  dataSource2: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  displayedColumns2: string[] = ['codigo','vacuna','lote', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  vacunasekeccionada= new Producto();
  vacunas:Producto[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha2=new FormControl(this.maxFecha);
  fecha3= new Date();
  fecha4= new Date();

  registroVacunas= new RegistroVacunas();


  constructor(private registrovacunasService:RegistrovacunasService,
    public dialogRef: MatDialogRef<VacunaciondialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RegistroVacunas,
    private pacienteanimalservice:PacienteanimalService,
    private productoService:ProductoService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'fechasiguiente': this.fecha1,
        'fechaaplicacion': this.fecha2,
        'peso': new FormControl(),
        'cantidad': new FormControl(),
        'vacuna': new FormControl(),
        'lote': new FormControl(),
        'laboratorio': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {
    this.listarvacunas();

    this.registroVacunas = { ...this.data };

    if(this.registroVacunas.idregistrovacuna!= null){
      this.form.patchValue({
        id:this.data.idregistrovacuna,
        fechasiguiente:this.data.fechasiguiente,
        fechaaplicacion:this.data.fechaaplicacion,
        peso:this.data.peso,
        paciente:this.data.pacienteanimal.nombre,
        cantidad:this.data.cantidad,
        vacuna:this.data.vacuna,
        lote:this.data.lote,
        laboratorio:this.data.laboratorio,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fechaaplicacion)
      this.fecha2.setValue(this.data.fechasiguiente)
    }

  }

  listarvacunas(){
    this.productoService.buscarporsubcategoria('VACUNAS').subscribe(data=>{
      console.log(data)
      this.dataSource2= new MatTableDataSource(data);
    })
  }

  aceptar(){
    let registrovacunas1 = new RegistroVacunas();
    registrovacunas1.idregistrovacuna=this.form.value['id']
    registrovacunas1.laboratorio = this.form.value['laboratorio'];
    registrovacunas1.lote = this.form.value['lote'];
    registrovacunas1.observaciones = this.form.value['observaciones'];
    registrovacunas1.peso = this.form.value['peso'];
    registrovacunas1.cantidad = 1;
    registrovacunas1.vacuna = this.form.value['vacuna'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    registrovacunas1.fechaaplicacion = localisotime.toISOString();

    this.fecha4 = new Date(this.fecha2.value);
    var tzoffset = (this.fecha4).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha4.getTime()-tzoffset));
    registrovacunas1.fechasiguiente = localisotime.toISOString();

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    registrovacunas1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;


    this.registroVacunas=registrovacunas1;
    console.log(this.registroVacunas)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.registroVacunas != null && this.registroVacunas.idregistrovacuna > 0 ) {
        //MODIFICAR
        this.registrovacunasService.modificar(this.registroVacunas).pipe(switchMap(() => {
          return this.registrovacunasService.listar()
        }))
        .subscribe(data => {
          this.registrovacunasService.setRegistroVacunasCambio(data);
          console.log(data)
          this.registrovacunasService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.registrovacunasService.registrar(this.registroVacunas).subscribe(() => {
          this.registrovacunasService.listar().subscribe(data => {
            this.registrovacunasService.setRegistroVacunasCambio(data);
            this.registrovacunasService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.registrovacunasService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }

  agregarvacuna(row:Producto){
    this.vacunasekeccionada=row;
    this.form.patchValue({
      vacuna:this.vacunasekeccionada.nombre,
    })
    this.snakbar.open('La vacuna se seleccionó correctamente','Aviso',{duration:1000});
  }

}
