import { Component, OnInit, ViewChild } from '@angular/core';
import { Comprobante } from '../../_model/comprobante';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ComprobanteService } from '../../_service/comprobante.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-resumenventas',
  templateUrl: './resumenventas.component.html',
  styleUrls: ['./resumenventas.component.css']
})
export class ResumenventasComponent implements OnInit {


  maxFecha: Date = new Date();
  fechaingreso:Date;
  fechainicio=new FormControl(this.maxFecha);
  fechafin= new FormControl(this.maxFecha);

  pdfSrc: string;
  useBrowserLocale:true;
  cp:any='';

  displayedColumns = ['idcomprobante','correlativo','tipocomprobante', 'total','fechaemision','estadointerno','acciones'];
  dataSource: MatTableDataSource<Comprobante>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private comprobanteService: ComprobanteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.comprobanteService.getComprobanteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.comprobanteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    let f1:Date = this.fechainicio.value;
    let f2:Date =this.fechafin.value;

    let diaEnMilisegundos = 1000 * 60 * 60 * 24 ;
    let m = moment(f1).format('YYYY-MM-DD');
    let n =moment(f2.getTime()+diaEnMilisegundos).format('YYYY-MM-DD');

    this.comprobanteService.comprobanteporfechas(m,n).subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Comprobante[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(comprobante?: Comprobante) {
    // this.dialog.open(ComprobanteDialogoComponent, {
    //   maxWidth: '90%',
    //   minWidth: '90%',
    //   data: comprobante
    // });
  }

  eliminar(comprobante: Comprobante) {
    this.comprobanteService.eliminar(comprobante.idcomprobante).pipe(switchMap(() => {
      return this.comprobanteService.listar();
    }))
      .subscribe(data => {
        this.comprobanteService.setMensajeCambio("SE ELIMINO");
        this.comprobanteService.setComprobanteCambio(data);
      });
  }

  buscarfecha(){
    let f1:Date = this.fechainicio.value;
    let f2:Date =this.fechafin.value;

    let diaEnMilisegundos = 1000 * 60 * 60 * 24 ;

    let m = moment(f1).format('YYYY-MM-DD');
    let n =moment(f2.getTime()+diaEnMilisegundos).format('YYYY-MM-DD');

    this.comprobanteService.comprobanteporfechas(m,n).subscribe(data=>{
      this.crearTabla(data);
    })
  }


  reportecaja(){
    let f1:Date = this.fechainicio.value;
    let f2:Date =this.fechafin.value;

    let diaEnMilisegundos = 1000 * 60 * 60 * 24 ;
    let m = moment(f1).format('YYYY-MM-DD');
    let n =moment(f2.getTime()+diaEnMilisegundos).format('YYYY-MM-DD');
    this.comprobanteService.reportecajaporfecha(m,n).subscribe(data=>{
      this.cp=data;
    })
  }

}
