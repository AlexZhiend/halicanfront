import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DetalleComprobante } from '../../../_model/detallecomprobante';
import { Comprobante } from '../../../_model/comprobante';
import { EmpresaService } from '../../../_service/empresa.service';
import { Moneda } from '../../../_model/moneda';
import { MonedaService } from '../../../_service/moneda.service';
import { Serie } from '../../../_model/serie';
import { SerieService } from '../../../_service/serie.service';
import { TipoComprobante } from '../../../_model/tipocomprobante';
import { TipocomprobanteService } from '../../../_service/tipocomprobante.service';
import { MatSelectChange } from '@angular/material/select';
import { MatTableDataSource } from '@angular/material/table';
import { EstadointernoService } from '../../../_service/estadointerno.service';
import { EstadoInterno } from '../../../_model/estadointerno';
import { ProductoService } from '../../../_service/producto.service';
import { ComprobanteService } from '../../../_service/comprobante.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ImpresionComponent } from './impresion/impresion.component';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmacionComponent implements OnInit {

  form: FormGroup;
  edicion:boolean=false;
  @Output() edicionventana:EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() comprobante:Comprobante;

  monedas: Moneda[]=[];
  monedaSeleccionado:Moneda;
  total:number;
  descuento:number=0;

  series:Serie[]=[];
  serieSeleccionado:Serie;

  estadosinternos:EstadoInterno[]=[];
  estadointernoSeleccionado:EstadoInterno;

  opgravadas:number;
  igv:number;
  importetotal:number=0;
  efectivo:number=0;
  correlativo:number;
  pdfSrc: string;

  tcomprobantes:TipoComprobante[]=[];
  tcomprobanteSeleccionado:TipoComprobante;

  transactions: DetalleComprobante[] = [];
  displayedColumns = ['item','descripcion','cantidad','importetotal'];
  dataSource:MatTableDataSource<DetalleComprobante>;

  formasdepago=['Efectivo','Transferencia','Visa'];
  form2:FormGroup;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private empresaService: EmpresaService,
              private monedaservice:MonedaService,
              private serieService:SerieService,
              private tipocomprobanteService:TipocomprobanteService,
              private estadointernoService:EstadointernoService,
              private productoService: ProductoService,
              private comprobanteservice: ComprobanteService,
              private snack: MatSnackBar,
              private dialog: MatDialog) {
                this.form=new FormGroup({
                  'id': new FormControl(0),
                  'denominacion': new FormControl(''),
                  'moneda':new FormControl(),
                  'serie':new FormControl(),
                  'tipocomprobante':new FormControl(),
                });
                this.form2=new FormGroup({
                  'metododepago': new FormControl('Efectivo'),
                  'estadointerno': new FormControl(''),
                })
               }

  ngOnInit(): void {
    this.buscarempresa();
    this.buscarmonedas();
    this.buscarTipocomprobante();
    this.listarestado();

    this.transactions = this.comprobante.detallecomprobante;
    this.dataSource=new MatTableDataSource(this.transactions);
    this.getTotalCost();
    this.efectivo=this.importetotal-this.descuento
  }

  listarestado(){
    this.estadointernoService.listar().subscribe(data=>{
      this.estadosinternos=data;
      for (let index = 0; index < this.estadosinternos.length; index++) {
        if (this.estadosinternos[index].descripcion=='Pagado') {
          this.form2.patchValue({
            estadointerno:this.estadosinternos[index].descripcion
          })
        }
      }
    })
  }

  getTotalCost() {
    this.total=this.transactions.map(t => t.importetotal).reduce((acc, value) => acc + value, 0);
    this.opgravadas=this.transactions.map(t => t.valortotal).reduce((acc, value) => acc + value, 0)
    this.igv=this.transactions.map(t => t.igv).reduce((acc, value) => acc + value, 0)
    this.importetotal=this.total;
    return this.transactions.map(t => t.importetotal).reduce((acc, value) => acc + value, 0);
  }

  operar(){

  }

  onVentaSeleccionado() {
    this.edicionventana.emit(this.edicion);
  }

  cerrar(){
    this.onVentaSeleccionado();
    this.router.navigate(['/pages/ventas']);
  }

  buscarempresa(){
    this.empresaService.listar().subscribe(data=>{
      let empresaselecccionada = data[0];
      this.comprobante.empresa=empresaselecccionada;
    })
  }

  buscarmonedas(){
    this.monedaservice.listar().subscribe(data=>{
      this.monedas=data;
      this.monedaSeleccionado=this.monedas[0];
      this.form.patchValue({
        moneda:this.monedas[0]
      })
      this.comprobante.moneda=this.monedaSeleccionado;
    })
  }

  buscarTipocomprobante(){
    this.tipocomprobanteService.buscarcodigosunat('12').subscribe(data=>{
      this.tcomprobanteSeleccionado=data;
      this.comprobante.tipocomprobante=this.tcomprobanteSeleccionado;
        this.serieService.buscarserietipo(data.idtipocomprobante).subscribe(data=>{
        this.series=data;
        this.serieSeleccionado=this.series[0];
        this.form.patchValue({
          serie:this.series[0]
        })
      this.comprobante.serie=this.serieSeleccionado;
    })
    })

    this.tipocomprobanteService.listar().subscribe(data=>{
      this.tcomprobantes=data;
      this.form.patchValue({
        tipocomprobante:this.tcomprobantes.find(tipo => tipo.codigotipocomprobante=='12')
      })
    })
  }

  filtroTipoComprobanteSerie(event: MatSelectChange) {
    this.tcomprobanteSeleccionado=event.source.value;
    this.serieService.buscarserietipo(this.tcomprobanteSeleccionado.idtipocomprobante).subscribe(data=>{
      this.series=data;
      this.serieSeleccionado=this.series[0];
      this.form.patchValue({
        serie:this.series[0]
      })
    })
  }

  verificar(){
    this.comprobante.moneda=this.form.value['moneda'];
    this.comprobante.tipocomprobante=this.form.value['tipocomprobante'];
    this.comprobante.serie=this.form.value['serie'];
    this.comprobante.total=this.decimalAdjust('round',this.total-this.descuento,-2);
    this.comprobante.igv=this.decimalAdjust('round',this.total-this.total/(1.18),-2);
    this.comprobante.opgravadas=this.decimalAdjust('round',this.total-this.comprobante.igv,-2);
    this.comprobante.descuento=this.decimalAdjust('round',this.descuento,-2);
    this.comprobante.formapago='CONTADO';
    this.comprobante.estadointerno=this.form2.value['estadointerno'];
    this.comprobante.metododepago=this.form2.value['metododepago'];

    let serie = new Serie();
    serie=this.form.value['serie'];
    this.comprobante.serienombre=serie.serie;

    // let tipocomp = new TipoComprobante();
    // tipocomp=this.form.value['tipocomprobante'];

    let detalles: DetalleComprobante[];
    detalles=this.comprobante.detallecomprobante;

    this.serieService.buscarcorrelativo(this.comprobante.tipocomprobante.codigotipocomprobante,
      this.comprobante.serie.idserie).subscribe(data=>{
      console.log(data)

      this.comprobante.correlativo=data.correlativo+1;
      let serienueva = new Serie();
      serienueva=data;
      serienueva.correlativo=data.correlativo+1;
      this.serieService.modificar(serienueva).subscribe(data=>{})

      this.comprobanteservice.registrar(this.comprobante).subscribe(data=>{
        console.log(data);
        this.abrirDialogo(this.comprobante);
        for (let i = 0; i < detalles.length; i++) {
          this.productoService.listarPorId(detalles[i].producto.idproducto).subscribe(data=>{

            if (detalles[i].producto.categoriaproducto.nombre=='Productos') {
              data.cantidad = data.cantidad-detalles[i].cantidad;
              this.productoService.modificar(data).subscribe(dataa=>{});
            }
          });}
        this.snack.open('Se registró correctamente',"OK",{duration: 2000});

      })
    })

  }

  decimalAdjust(type:any, value:any, exp:any) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math['round'](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math['round'](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  cargardetalle(){
    this.importetotal=this.total;
  }

  // generarpdf(){
  //   this.comprobanteservice.generarTicket(1,6).subscribe(data => {
  //     let reader = new FileReader();
  //     reader.onload = (e: any) => {
  //       this.pdfSrc = e.target.result;
  //       console.log(this.pdfSrc);
  //     }
  //     reader.readAsArrayBuffer(data);

  //     const url = window.URL.createObjectURL(data);
  //     const a = document.createElement('a');
  //     a.setAttribute('style', 'display:none');
  //     document.body.appendChild(a);
  //     a.href = url;
  //     a.download = 'archivo.pdf';
  //     a.click();
  //   });
  // }

  abrirDialogo(comp?: Comprobante) {
    this.dialog.open(ImpresionComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: comp,
      disableClose:true
    });
  }

}



