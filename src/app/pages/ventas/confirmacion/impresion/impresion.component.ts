import { Component, OnInit, Inject } from '@angular/core';
import { ComprobanteService } from '../../../../_service/comprobante.service';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import { Comprobante } from '../../../../_model/comprobante';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmacionComponent } from '../confirmacion.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-impresion',
  templateUrl: './impresion.component.html',
  styleUrls: ['./impresion.component.css']
})
export class ImpresionComponent implements OnInit {

  pdfSrc: string;
  useBrowserLocale:true;
  cp:any='';

  constructor(private comprobanteservice:ComprobanteService,
              private dialogRef: MatDialogRef<ConfirmacionComponent>,
              private snackbar:MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: Comprobante) {
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
   }

  ngOnInit(): void {
    this.generarpdf();
  }

    generarpdf(){
    this.comprobanteservice.generarTicket(this.data.correlativo,this.data.serie.idserie).
    subscribe(data => {
      this.cp=data;
    });
  }

  cerrar(){
    this.snackbar.open('Se canceló el procedimiento','AVISO',{duration: 2000});
    window.location.reload();
  }

}
