import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProductoService } from '../../_service/producto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Producto } from '../../_model/producto';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, switchMap } from 'rxjs';
import { DetalleComprobante } from '../../_model/detallecomprobante';
import { ClienteService } from '../../_service/cliente.service';
import { SearchConsultaDTO } from '../../_model/searchConsultaDTO';
import { MatDialog } from '@angular/material/dialog';
import { BusquedaclienteComponent } from './busquedacliente/busquedacliente.component';
import { Cliente } from 'src/app/_model/cliente';
import { MatInput } from '@angular/material/input';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Comprobante } from '../../_model/comprobante';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css'],
})
export class VentasComponent implements OnInit {

  dataSource: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['codigobarras', 'nombre', 'cantidad',
  'fechavencimiento','valorunitario','acciones'];
  cantidad: number;

  maxFecha: Date = new Date();
  fechaingreso:Date;
  fechaemision=new FormControl(this.maxFecha);
  fechavencimiento= new FormControl(this.maxFecha);

  detalle= new DetalleComprobante();
  detalles:DetalleComprobante[]= [];

  dataSource2: MatTableDataSource<DetalleComprobante>;
  displayedColumns2: string[] = ['descripcion', 'cantidad',
  'preciounitario','valortotal','acciones'];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  clienteseleccionado= new Cliente();

  @ViewChild('search') searchElement: ElementRef;

  formproducto:FormGroup;

  productos:Producto[]=[];
  estado:boolean=false;
  estadoventana:boolean=true;

  //tablas
  displayedColumns5 = ['position','descripcion',
  'preciounitario','cantidad','importetotal','acciones'];
  dataSource5:MatTableDataSource<DetalleComprobante>;
  comprobante:Comprobante= new Comprobante();

  constructor(
    private productoService: ProductoService,
    private snackBar: MatSnackBar,
    private clienteService:ClienteService,
    private dialog: MatDialog,
      ) { this.formproducto=new FormGroup({
        'nombreocod': new FormControl(null),
      });
     }

  ngOnInit(): void {

    let searchConsultaDTO=new SearchConsultaDTO('CLIENTE') ;
    this.clienteService.BuscarNombreDNI(searchConsultaDTO).subscribe(data => {
      this.clienteseleccionado=data[0]
    });

    this.productoService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.productoService.getProductoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.productoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    this.clienteService.getClienteadd().subscribe(data=>{
      this.clienteseleccionado=data

    })
  }


  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Producto[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.productoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  vencimiento(e:Date){
    var fechaactual = new Date();
    let mesEnMilisegundos = 1000 * 60 * 60 * 24 * 30;
    var m = moment(e).toDate();
    if (m.getTime()<=fechaactual.getTime()) {
      return 1;
    } else if (m.getTime()>fechaactual.getTime() &&
     m.getTime()<fechaactual.getTime()+mesEnMilisegundos){
      return 2;
    }
    else{
      return 3
    }
  }

  Buscarcliente(){
      this.dialog.open(BusquedaclienteComponent, {
        maxWidth: '70%',
        disableClose:false,
        minWidth: '70%'
      });
  }

  buscarproducto(){
    let e = this.formproducto.value['nombreocod'];
    if (e!=null) {
      this.productoService.buscarnombrecod(e).subscribe(data=>{
        this.dataSource=new MatTableDataSource(data);
      })
    } else {
      this.snackBar.open('Ingrese el código ó nombre del producto','Aviso',{duration:2000})
    }
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key == 'control') {
      console.log('se imprimira')
    } else {
    }
  }

  showSearch(e:Producto){
    setTimeout(()=>{
      this.searchElement.nativeElement.focus();
    },0);
    this.dataSource= new MatTableDataSource();
    var detalleseleccionado = new DetalleComprobante();
    detalleseleccionado.producto=e;
    detalleseleccionado.descripcion=e.nombre;
    detalleseleccionado.cantidad=1;

    detalleseleccionado.importetotal=e.valorunitario*detalleseleccionado.cantidad;
    detalleseleccionado.descuentoitem=0;
    detalleseleccionado.preciounitario=e.valorunitario;
    detalleseleccionado.item=this.detalles.length+1;

    if (e.tipoafectacion.nombre =='IGV' &&(
        e.categoriaproducto.nombre=='Productos' ||
        e.categoriaproducto.nombre=='Servicios')) {
      detalleseleccionado.valorunitario=this.decimalAdjust('round',e.valorunitario/(1.18),-2);
      // detalleseleccionado.valortotal=detalleseleccionado.valorunitario*detalleseleccionado.cantidad;
      // detalleseleccionado.igv=detalleseleccionado.importetotal-detalleseleccionado.valortotal;
      detalleseleccionado.porcentajeigv=18;
    }else{
      detalleseleccionado.valorunitario=e.valorunitario;
      detalleseleccionado.igv=0;
      detalleseleccionado.porcentajeigv=0;
      // detalleseleccionado.valortotal=detalleseleccionado.valorunitario*detalleseleccionado.cantidad;
    }
    this.detalles.push(detalleseleccionado);
    this.dataSource5= new MatTableDataSource(this.detalles);
    this.formproducto.reset();
  }

  decimalAdjust(type:any, value:any, exp:any) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math['round'](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math['round'](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  //se capturan los datos de la tabla de detalles nuevamente y se recalculan los datos
  verificar(){
    if (this.detalles.length!=0) {
      this.detalles.forEach(e => {
        e.importetotal=this.decimalAdjust('round',e.cantidad*e.preciounitario,-2);
        e.valortotal=this.decimalAdjust('round',e.valorunitario*e.cantidad,-2);
        e.igv=this.decimalAdjust('round',e.importetotal-e.valortotal,-2);
    });
    this.comprobante.detallecomprobante=this.detalles;
    this.comprobante.cliente=this.clienteseleccionado;

    this.comprobante.fechaemision=this.fechaemision.value;
    this.comprobante.fechavencimiento=this.fechavencimiento.value;

    this.estadoventana=false;
    }else{
      this.snackBar.open('Ingrese productos al carrito de compras','AVISO',{duration:2000})
    }
  }

  eliminardetalle(e:DetalleComprobante){
    var indice=this.detalles.indexOf(e);
    this.detalles.splice(indice, 1);
    for (let i = 0; i < this.detalles.length; i++) {
      this.detalles[i].item=i+1;
      console.log(this.detalles[i].item)
    }
    this.dataSource5 = new MatTableDataSource(this.detalles);
  }

  agregarconfirmacion(arg:boolean){
    this.snackBar.open('Se canceló la operacion, confirme el registro nuevamente','AVISO',{duration:6000})
    this.estadoventana=true;
  }

}


