import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClienteService } from '../../../_service/cliente.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Cliente } from '../../../_model/cliente';
import { switchMap } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VentasComponent } from '../ventas.component';
import { SearchConsultaDTO } from '../../../_model/searchConsultaDTO';

@Component({
  selector: 'app-busquedacliente',
  templateUrl: './busquedacliente.component.html',
  styleUrls: ['./busquedacliente.component.css'],
})


export class BusquedaclienteComponent implements OnInit {

  dataSource: MatTableDataSource<Cliente>;
  displayedColumns: string[] = ['idcliente', 'nombresyapellidos', 'dni', 'acciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  nombreodni: string;

  constructor(
    private clienteService: ClienteService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<VentasComponent>,
    @Inject(MAT_DIALOG_DATA) public data: String,
  ) { }

  ngOnInit(): void {

  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Cliente[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cerrar(){
    this.dialogRef.close();
  }

  Buscarcliente(){

    if (this.nombreodni!=null) {
      let searchConsultaDTO = new SearchConsultaDTO(this.nombreodni);
    this.clienteService.BuscarNombreDNI(searchConsultaDTO).subscribe(data => {
      if (data.length!=0) {
        this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort= this.sort;
      console.log(data)
      } else {
        this.snackBar.open('No se encontró el cliente', 'AVISO', {
          duration: 2000
        });
      }
    });

    this.clienteService.getClienteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.clienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
    } else {
      this.snackBar.open('Ingrese el nombre o el DNI', 'AVISO', {
        duration: 2000
      });
    }
  }

  agregar(row: Cliente){
    this.clienteService.setClienteadd(row);
    setTimeout(() => {
      this.dialogRef.close();
   }, 500)
   this.snackBar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000})
  }

}

