import { Component, OnInit, ViewChild } from '@angular/core';
import { TipoDocumento } from '../../_model/tipodocumento';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TipodocumentoService } from '../../_service/tipodocumento.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TipodocumentoDialogComponent } from './tipodocumento-dialog/tipodocumento-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipodocumento',
  templateUrl: './tipodocumento.component.html',
  styleUrls: ['./tipodocumento.component.css']
})
export class TipodocumentoComponent implements OnInit {


  displayedColumns = ['idtipodocumento', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<TipoDocumento>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tipodocumentoService: TipodocumentoService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.tipodocumentoService.getTipoDocumentoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tipodocumentoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.tipodocumentoService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: TipoDocumento[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(tipodocumento?: TipoDocumento) {
    this.dialog.open(TipodocumentoDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: tipodocumento
    });
  }

  eliminar(tipodocumento: TipoDocumento) {
    this.tipodocumentoService.eliminar(tipodocumento.idtipodocumento).pipe(switchMap(() => {
      return this.tipodocumentoService.listar();
    }))
      .subscribe(data => {
        this.tipodocumentoService.setMensajeCambio("SE ELIMINO");
        this.tipodocumentoService.setTipoDocumentoCambio(data);
      });
  }
}
