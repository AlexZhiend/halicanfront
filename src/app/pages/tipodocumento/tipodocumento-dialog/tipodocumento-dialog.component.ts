import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { TipoDocumento } from '../../../_model/tipodocumento';
import { TipodocumentoService } from '../../../_service/tipodocumento.service';

@Component({
  selector: 'app-tipodocumento-dialog',
  templateUrl: './tipodocumento-dialog.component.html',
  styleUrls: ['./tipodocumento-dialog.component.css']
})
export class TipodocumentoDialogComponent implements OnInit {

  tipodocumento: TipoDocumento;

  constructor(
    private dialogRef: MatDialogRef<TipodocumentoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: TipoDocumento,
    private tipodocumentoService : TipodocumentoService,
  ) { }

  ngOnInit(): void {
    this.tipodocumento = { ...this.data };
  }

  operar() {
    if (this.tipodocumento.descripcion!='') {
      if (this.tipodocumento != null && this.tipodocumento.idtipodocumento > 0 ) {
        //MODIFICAR
        this.tipodocumentoService.modificar(this.tipodocumento).pipe(switchMap(() => {
          return this.tipodocumentoService.listar();
        }))
        .subscribe(data => {
          this.tipodocumentoService.setTipoDocumentoCambio(data);
          this.tipodocumentoService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tipodocumentoService.registrar(this.tipodocumento).subscribe(() => {
          this.tipodocumentoService.listar().subscribe(data => {
            this.tipodocumentoService.setTipoDocumentoCambio(data);
            this.tipodocumentoService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tipodocumentoService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar() {
    this.dialogRef.close();
  }
}
