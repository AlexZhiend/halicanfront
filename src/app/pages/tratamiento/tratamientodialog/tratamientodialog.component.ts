import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormControl } from '@angular/forms';
import { TratamientoService } from '../../../_service/tratamiento.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tratamiento } from '../../../_model/tratamiento';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tratamientodialog',
  templateUrl: './tratamientodialog.component.html',
  styleUrls: ['./tratamientodialog.component.css']
})
export class TratamientodialogComponent implements OnInit {


  panelOpenState=false;
  @ViewChild(MatAccordion) accordion:MatAccordion;

  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha2=new FormControl(this.maxFecha);
  fecha3= new Date();
  fecha4= new Date();

  tratamiento= new Tratamiento();

  estados=['EN PROCESO','FINALIZADO'];
  estadoSeleccionado:string;

  constructor(private tratamientoService:TratamientoService,
    public dialogRef: MatDialogRef<TratamientodialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Tratamiento,
    private pacienteanimalservice:PacienteanimalService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'fechainicio': this.fecha1,
        'fechafin': this.fecha2,
        'descripcion': new FormControl(),
        'estado': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {

    this.tratamiento = { ...this.data };

    if(this.tratamiento.idtratamiento!= null){
      this.form.patchValue({
        id:this.data.idtratamiento,
        fechainicio:this.data.fechainicio,
        fechafin:this.data.fechafin,
        descripcion:this.data.descripcion,
        paciente:this.data.pacienteanimal.nombre,
        estado:this.data.estado,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fechainicio)
      this.fecha2.setValue(this.data.fechafin)
    }

  }

  aceptar(){
    let tratamiento1 = new Tratamiento();
    tratamiento1.idtratamiento=this.form.value['id']
    tratamiento1.observaciones = this.form.value['observaciones'];
    tratamiento1.descripcion = this.form.value['descripcion'];
    tratamiento1.estado = this.form.value['estado'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    tratamiento1.fechainicio = localisotime.toISOString();

    this.fecha4 = new Date(this.fecha2.value);
    var tzoffset = (this.fecha4).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha4.getTime()-tzoffset));
    tratamiento1.fechafin = localisotime.toISOString();

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    tratamiento1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;


    this.tratamiento=tratamiento1;
    console.log(this.tratamiento)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.tratamiento != null && this.tratamiento.idtratamiento > 0 ) {
        //MODIFICAR
        this.tratamientoService.modificar(this.tratamiento).pipe(switchMap(() => {
          return this.tratamientoService.listar()
        }))
        .subscribe(data => {
          this.tratamientoService.setTratamientoCambio(data);
          console.log(data)
          this.tratamientoService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tratamientoService.registrar(this.tratamiento).subscribe(() => {
          this.tratamientoService.listar().subscribe(data => {
            this.tratamientoService.setTratamientoCambio(data);
            this.tratamientoService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tratamientoService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }

}
