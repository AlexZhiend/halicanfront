import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Tratamiento } from '../../_model/tratamiento';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TratamientoService } from '../../_service/tratamiento.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { TratamientodialogComponent } from './tratamientodialog/tratamientodialog.component';

@Component({
  selector: 'app-tratamiento',
  templateUrl: './tratamiento.component.html',
  styleUrls: ['./tratamiento.component.css']
})
export class TratamientoComponent implements OnInit {


  dataSource: MatTableDataSource<Tratamiento>;
  displayedColumns: string[] = ['idtratamiento', 'paciente',
  'propietario','telefono','descripcion','estado','fechainicio', 'acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tratamientoService: TratamientoService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.tratamientoService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.tratamientoService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.tratamientoService.getTratamientoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tratamientoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.tratamientoService.eliminar(id).pipe(switchMap( ()=> {
      return this.tratamientoService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.tratamientoService.setTratamientoCambio(data);
      this.tratamientoService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Tratamiento[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.tratamientoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(tratamiento?: Tratamiento) {
    this.dialog.open(TratamientodialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: tratamiento
    });
  }

}
