import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardService } from '../_service/guard.service';
import { CategoriaComponent } from './categoria/categoria.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { EstadointernoComponent } from './estadointerno/estadointerno.component';
import { InicioComponent } from './inicio/inicio.component';
import { Not403Component } from './not403/not403.component';
import { ExamenmedicoComponent } from './examenmedico/examenmedico.component';
import { MonedaComponent } from './moneda/moneda.component';
import { ProveedorComponent } from './proveedor/proveedor.component';
import { TipoafectacionComponent } from './tipoafectacion/tipoafectacion.component';
import { UnidadComponent } from './unidad/unidad.component';
import { TipobanioComponent } from './tipobanio/tipobanio.component';
import { TipocorteComponent } from './tipocorte/tipocorte.component';
import { TipodocumentoComponent } from './tipodocumento/tipodocumento.component';
import { PersonalComponent } from './personal/personal.component';
import { ClienteComponent } from './cliente/cliente.component';
import { PacienteanimalComponent } from './pacienteanimal/pacienteanimal.component';
import { ProductoComponent } from './producto/producto.component';
import { VentasComponent } from './ventas/ventas.component';
import { TipocomprobanteComponent } from './tipocomprobante/tipocomprobante.component';
import { SerieComponent } from './serie/serie.component';
import { ConfirmacionComponent } from './ventas/confirmacion/confirmacion.component';
import { ResumenventasComponent } from './resumenventas/resumenventas.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { BaniosComponent } from './banios/banios.component';
import { VacunacionComponent } from './vacunacion/vacunacion.component';
import { DesparasitacionComponent } from './desparasitacion/desparasitacion.component';
import { HospedajeComponent } from './hospedaje/hospedaje.component';
import { HospitalizacionComponent } from './hospitalizacion/hospitalizacion.component';
import { TratamientoComponent } from './tratamiento/tratamiento.component';
import { CirujiaComponent } from './cirujia/cirujia.component';

export const routes: Routes = [
  { path: 'inicio', component: InicioComponent, canActivate: [GuardService]},
  { path: 'categoriaproducto', component: CategoriaComponent, canActivate: [GuardService]},
  { path: 'empresa', component: EmpresaComponent, canActivate: [GuardService]},
  { path: 'estadointerno', component: EstadointernoComponent, canActivate: [GuardService]},
  { path: 'examenmedico', component: ExamenmedicoComponent, canActivate: [GuardService]},
  { path: 'moneda', component: MonedaComponent, canActivate: [GuardService]},
  { path: 'proveedor', component: ProveedorComponent, canActivate: [GuardService]},
  { path: 'tipoafectacion', component: TipoafectacionComponent, canActivate: [GuardService]},
  { path: 'unidad', component: UnidadComponent, canActivate: [GuardService]},
  { path: 'tipobanio', component: TipobanioComponent, canActivate: [GuardService]},
  { path: 'tipocorte', component: TipocorteComponent, canActivate: [GuardService]},
  { path: 'tipodocumento', component: TipodocumentoComponent, canActivate: [GuardService]},
  { path: 'personalveterinario', component: PersonalComponent, canActivate: [GuardService]},
  { path: 'cliente', component: ClienteComponent, canActivate: [GuardService]},
  { path: 'paciente', component: PacienteanimalComponent, canActivate: [GuardService]},
  { path: 'productos', component: ProductoComponent, canActivate: [GuardService]},
  { path: 'ventas', component: VentasComponent, canActivate: [GuardService],
          children:[{path:'confirmacionventa',component:ConfirmacionComponent}
        ]},
  { path: 'resumenventas', component: ResumenventasComponent, canActivate: [GuardService]},
  { path: 'series', component: SerieComponent, canActivate: [GuardService]},
  { path: 'tiposcomprobante', component: TipocomprobanteComponent, canActivate: [GuardService]},
  { path: 'consulta', component: ConsultaComponent, canActivate: [GuardService]},
  { path: 'registrodebanio', component: BaniosComponent, canActivate: [GuardService]},
  { path: 'vacunaciones', component: VacunacionComponent, canActivate: [GuardService]},
  { path: 'desparasitaciones', component: DesparasitacionComponent, canActivate: [GuardService]},
  { path: 'hospedaje', component: HospedajeComponent, canActivate: [GuardService]},
  { path: 'hospitalizaciones', component: HospitalizacionComponent, canActivate: [GuardService]},
  { path: 'tratamiento', component: TratamientoComponent, canActivate: [GuardService]},
  { path: 'cirujias', component: CirujiaComponent, canActivate: [GuardService]},
  { path: 'not-403', component: Not403Component}


  // {
    //     path: 'paciente', component: PacienteComponent, children: [
    //         { path: 'nuevo', component: PacienteEdicionComponent },
    //         { path: 'edicion/:id', component: PacienteEdicionComponent }
    //     ], canActivate: [GuardService]
    // },
    // {
    //     path: 'examen', component: ExamenComponent, children: [
    //         { path: 'nuevo', component: ExamenEdicionComponent },
    //         { path: 'edicion/:id', component: ExamenEdicionComponent }
    //     ], canActivate: [GuardService]
    // },
    // {
    //     path: 'especialidad', component: EspecialidadComponent, children: [
    //         { path: 'nuevo', component: EspecialidadEdicionComponent },
    //         { path: 'edicion/:id', component: EspecialidadEdicionComponent }
    //     ], canActivate: [GuardService]
    // },
    // { path: 'signos-vitales', component: SignosVitalesComponent, children:[
    //   { path: 'nuevo', component: SignosVitalesEdicionComponent },
    //   { path: 'edicion/:id', component: SignosVitalesEdicionComponent }]
    // ,canActivate: [GuardService] },

    // { path: 'medico', component: MedicoComponent, canActivate: [GuardService]},
    // { path: 'consulta', component: ConsultaComponent, canActivate: [GuardService] },
    // { path: 'consulta-wizard', component: ConsultaWizardComponent, canActivate: [GuardService] },
    // { path: 'consulta-especial', component: ConsultaEspecialComponent, canActivate: [GuardService] },
    // { path: 'buscar', component: BuscarComponent, canActivate: [GuardService] },
    // { path: 'reporte', component: ReporteComponent, canActivate: [GuardService] },
    // { path: 'perfil', component: PerfilComponent, canActivate: [GuardService] },
    // { path: 'not-403', component: Not403Component}

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
