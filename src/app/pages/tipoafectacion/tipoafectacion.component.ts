import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { TipoAfectacion } from '../../_model/tipoafectacion';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TipoafectacionService } from '../../_service/tipoafectacion.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TipoafectacionDialogComponent } from './tipoafectacion-dialog/tipoafectacionr-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipoafectacion',
  templateUrl: './tipoafectacion.component.html',
  styleUrls: ['./tipoafectacion.component.css']
})
export class TipoafectacionComponent implements OnInit {

  displayedColumns = ['nombre','letra','codigo','tipo', 'acciones'];
  dataSource: MatTableDataSource<TipoAfectacion>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tipoafectacionService: TipoafectacionService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.tipoafectacionService.getTipoAfectacionCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tipoafectacionService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.tipoafectacionService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: TipoAfectacion[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(tipoafectacion?: TipoAfectacion) {
    this.dialog.open(TipoafectacionDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: tipoafectacion
    });
  }

  eliminar(tipoafectacion: TipoAfectacion) {
    this.tipoafectacionService.eliminar(tipoafectacion.idtipoafectacion).pipe(switchMap(() => {
      return this.tipoafectacionService.listar();
    }))
      .subscribe(data => {
        this.tipoafectacionService.setMensajeCambio("SE ELIMINO");
        this.tipoafectacionService.setTipoAfectacionCambio(data);
      });
  }

}
