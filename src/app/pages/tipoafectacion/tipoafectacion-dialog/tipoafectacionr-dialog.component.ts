import { Component, OnInit, Inject } from '@angular/core';
import { TipoAfectacion } from '../../../_model/tipoafectacion';
import { TipoafectacionComponent } from '../tipoafectacion.component';
import { TipoafectacionService } from '../../../_service/tipoafectacion.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipoafectacionr-dialog',
  templateUrl: './tipoafectacionr-dialog.component.html',
  styleUrls: ['./tipoafectacionr-dialog.component.css']
})
export class TipoafectacionDialogComponent implements OnInit {


  tipoafectacion: TipoAfectacion;

  constructor(
    private dialogRef: MatDialogRef<TipoafectacionComponent>,
    @Inject(MAT_DIALOG_DATA) private data: TipoAfectacion,
    private tipoafectacionService : TipoafectacionService,
  ) { }

  ngOnInit(): void {
    this.tipoafectacion = { ...this.data };
  }

  operar() {
    if (this.tipoafectacion.descripcion!='' && this.tipoafectacion.letra!='' &&
    this.tipoafectacion.codigo!='' && this.tipoafectacion.nombre!='' && this.tipoafectacion.tipo!='') {
      if (this.tipoafectacion != null && this.tipoafectacion.idtipoafectacion > 0 ) {
        //MODIFICAR
        this.tipoafectacionService.modificar(this.tipoafectacion).pipe(switchMap(() => {
          return this.tipoafectacionService.listar();
        }))
        .subscribe(data => {
          this.tipoafectacionService.setTipoAfectacionCambio(data);
          this.tipoafectacionService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tipoafectacionService.registrar(this.tipoafectacion).subscribe(() => {
          this.tipoafectacionService.listar().subscribe(data => {
            this.tipoafectacionService.setTipoAfectacionCambio(data);
            this.tipoafectacionService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tipoafectacionService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar() {
    this.dialogRef.close();
  }
}
