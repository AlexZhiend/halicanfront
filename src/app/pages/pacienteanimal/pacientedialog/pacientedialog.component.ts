import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from '../../../_model/cliente';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClienteService } from '../../../_service/cliente.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalComponent } from '../pacienteanimal.component';
import { SearchConsultaDTO } from '../../../_model/searchConsultaDTO';
import { MatAccordion } from '@angular/material/expansion';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { switchMap } from 'rxjs';
import * as moment from 'moment';


@Component({
  selector: 'app-pacientedialog',
  templateUrl: './pacientedialog.component.html',
  styleUrls: ['./pacientedialog.component.css']
})
export class PacientedialogComponent implements OnInit {
  panelOpenState = false;
  @ViewChild(MatAccordion) accordion: MatAccordion;

  tipodocumentos: Cliente[]=[];
  maxFecha: Date = new Date();
  fnacimiento=new Date();

  pacienteanimal=new PacienteAnimal();
  clienteseleccionado= new Cliente();

  form:FormGroup;
  MycontrolTipo= new FormControl();

  tipodocumentoseleccionado:number;

  //cliente
  dataSource: MatTableDataSource<Cliente>;
  displayedColumns: string[] = ['idcliente', 'nombresyapellidos', 'dni', 'acciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  nombreodni: string;

  sanimal=['HEMBRA','MACHO'];
  parametrica=['SI','NO'];
  especies=['PERRO','GATO','AVE','ROEDOR','OTRO'];


  constructor( private clienteService: ClienteService,
    private pacienteanimalservice: PacienteanimalService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<PacienteanimalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PacienteAnimal) {

      this.form=new FormGroup({
        'numerohistoria': new FormControl(),
        'idpacienteanimal':new FormControl(),
        'nombre':new FormControl(),
        'raza':new FormControl(),
        'especie':new FormControl(),
        'color':new FormControl(),
        'esterilizacion':new FormControl(),
        'sexo':new FormControl(),
        'fechanacimiento':new FormControl(),
        'cliente':new FormControl(),

      });

     }

  ngOnInit(): void {
    this.pacienteanimal = { ...this.data };
    console.log(this.pacienteanimal);

    if(this.pacienteanimal.idpacienteanimal!= null){
      this.form.patchValue({
        idpacienteanimal:this.data.idpacienteanimal,
        numerohistoria:this.data.numerohistoria,
        nombre:this.data.nombre,
        raza:this.data.raza,
        especie:this.data.especie,
        color:this.data.color,
        esterilizacion:this.data.esterilizacion,
        sexo:this.data.sexo,
        fechanacimiento:this.data.fechanacimiento,
        cliente:this.data.cliente.nombresyapellidos,
      });
      this.clienteseleccionado=this.data.cliente;
    }
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Cliente[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cerrar(){
    this.dialogRef.close();
  }

  Buscarcliente(){

    if (this.nombreodni!=null) {
      let searchConsultaDTO = new SearchConsultaDTO(this.nombreodni);
    this.clienteService.BuscarNombreDNI(searchConsultaDTO).subscribe(data => {
      if (data.length!=0) {
        this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort= this.sort;
      console.log(data)
      } else {
        this.snackBar.open('No se encontró el cliente', 'AVISO', {
          duration: 2000
        });
      }
    });

    this.clienteService.getClienteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.clienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
    } else {
      this.snackBar.open('Ingrese el nombre o el DNI', 'AVISO', {
        duration: 2000
      });
    }
  }

  agregar(row: Cliente){
    this.clienteseleccionado=row;
    this.form.patchValue({
      cliente:this.clienteseleccionado.nombresyapellidos,
    })
    this.snackBar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
    this.panelOpenState=false;
  }

  //paciente animal
  aceptar(){
    this.pacienteanimal.nombre=this.form.value['nombre'];
    this.pacienteanimal.color=this.form.value['color'];
    this.pacienteanimal.especie=this.form.value['especie'];
    this.pacienteanimal.esterilizacion=this.form.value['esterilizacion'];
    this.pacienteanimal.fechanacimiento=moment(this.form.value['fechanacimiento']).format('YYYY-MM-DD');
    this.pacienteanimal.raza=this.form.value['raza'];
    this.pacienteanimal.sexo=this.form.value['sexo'];
    this.pacienteanimal.cliente=this.clienteseleccionado;

    if (this.form.valid == true) {
      if (this.pacienteanimal != null && this.pacienteanimal.idpacienteanimal > 0 ) {
        //MODIFICAR
        this.pacienteanimalservice.modificar(this.pacienteanimal).pipe(switchMap(() => {
          return this.pacienteanimalservice.listar()
        }))
        .subscribe(data => {
          this.pacienteanimalservice.setPacienteAnimalCambio(data);
          this.pacienteanimalservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.pacienteanimalservice.buscarUltimaHistoria().subscribe(data=>{
          if (data==null) {
            this.pacienteanimal.numerohistoria=1;
            this.pacienteanimalservice.registrar(this.pacienteanimal).subscribe(() => {
              this.pacienteanimalservice.listar().subscribe(data => {
                this.pacienteanimalservice.setPacienteAnimalCambio(data);
                this.pacienteanimalservice.setMensajeCambio("SE REGISTRO");
              });
            });
          }else{
            this.pacienteanimal.numerohistoria=data.numerohistoria+1;
            this.pacienteanimalservice.registrar(this.pacienteanimal).subscribe(() => {
              this.pacienteanimalservice.listar().subscribe(data => {
                this.pacienteanimalservice.setPacienteAnimalCambio(data);
                this.pacienteanimalservice.setMensajeCambio("SE REGISTRO");
              });
            });
          }


        })

      }
      this.cerrar();
    }else{
      this.pacienteanimalservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

}
