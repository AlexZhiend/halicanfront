import { Component, OnInit, ViewChild } from '@angular/core';
import { PacienteAnimal } from '../../_model/pacienteanimal';
import { MatTableDataSource } from '@angular/material/table';
import { PacienteanimalService } from '../../_service/pacienteanimal.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { PacientedialogComponent } from './pacientedialog/pacientedialog.component';

@Component({
  selector: 'app-pacienteanimal',
  templateUrl: './pacienteanimal.component.html',
  styleUrls: ['./pacienteanimal.component.css']
})
export class PacienteanimalComponent implements OnInit {

  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['numerohistoria','nombre','raza','propietario','telefono', 'acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  linkwhatsapp='';

  constructor(
    private pacienteanimalService: PacienteanimalService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.pacienteanimalService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.pacienteanimalService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.pacienteanimalService.getPacienteAnimalCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.pacienteanimalService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.pacienteanimalService.eliminar(id).pipe(switchMap( ()=> {
      return this.pacienteanimalService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.pacienteanimalService.setPacienteAnimalCambio(data);
      this.pacienteanimalService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: PacienteAnimal[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.pacienteanimalService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });
  }

  abrirDialogo(pacienteanimal?: PacienteAnimal) {
    this.dialog.open(PacientedialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: pacienteanimal
    });
  }

}
