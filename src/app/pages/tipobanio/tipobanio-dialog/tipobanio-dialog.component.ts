import { Component, OnInit, Inject } from '@angular/core';
import { TipoBanio } from '../../../_model/tipobanio';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TipobanioService } from '../../../_service/tipobanio.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipobanio-dialog',
  templateUrl: './tipobanio-dialog.component.html',
  styleUrls: ['./tipobanio-dialog.component.css']
})
export class TipobanioDialogComponent implements OnInit {

  tipobanio: TipoBanio;

  constructor(
    private dialogRef: MatDialogRef<TipobanioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: TipoBanio,
    private tipobanioService : TipobanioService,
  ) { }

  ngOnInit(): void {
    this.tipobanio = { ...this.data };
  }

  operar() {
    if (this.tipobanio.descripcion!='') {
      if (this.tipobanio != null && this.tipobanio.idtipobanio > 0 ) {
        //MODIFICAR
        this.tipobanioService.modificar(this.tipobanio).pipe(switchMap(() => {
          return this.tipobanioService.listar();
        }))
        .subscribe(data => {
          this.tipobanioService.setTipoBanioCambio(data);
          this.tipobanioService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tipobanioService.registrar(this.tipobanio).subscribe(() => {
          this.tipobanioService.listar().subscribe(data => {
            this.tipobanioService.setTipoBanioCambio(data);
            this.tipobanioService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tipobanioService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar() {
    this.dialogRef.close();
  }

}
