import { Component, OnInit, ViewChild } from '@angular/core';
import { TipoBanio } from 'src/app/_model/tipobanio';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TipobanioService } from '../../_service/tipobanio.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TipobanioDialogComponent } from './tipobanio-dialog/tipobanio-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipobanio',
  templateUrl: './tipobanio.component.html',
  styleUrls: ['./tipobanio.component.css']
})
export class TipobanioComponent implements OnInit {


  displayedColumns = ['idtipobanio', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<TipoBanio>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tipobanioService: TipobanioService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.tipobanioService.getTipoBanioCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tipobanioService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.tipobanioService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: TipoBanio[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(tipobanio?: TipoBanio) {
    this.dialog.open(TipobanioDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: tipobanio
    });
  }

  eliminar(tipobanio: TipoBanio) {
    this.tipobanioService.eliminar(tipobanio.idtipobanio).pipe(switchMap(() => {
      return this.tipobanioService.listar();
    }))
      .subscribe(data => {
        this.tipobanioService.setMensajeCambio("SE ELIMINO");
        this.tipobanioService.setTipoBanioCambio(data);
      });
  }
}
