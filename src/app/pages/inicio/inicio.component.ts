import { hostViewClassName } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { RegistroDesparasitacion } from 'src/app/_model/registrodesparasitacion';
import { MenuService } from 'src/app/_service/menu.service';
import { environment } from 'src/environments/environment';
import { Consulta } from '../../_model/consulta';
import { ConsultaService } from '../../_service/consulta.service';
import { RegistrodesparasitacionService } from '../../_service/registrodesparasitacion.service';
import { SerieService } from '../../_service/serie.service';
import { Serie } from '../../_model/serie';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  usuario: string='';
  reg: Serie[]=[];

  constructor(
    private menuService: MenuService,
    private consultaservice: SerieService
  ) { }

  ngOnInit(): void {
    const helper = new JwtHelperService();

    let token = JSON.stringify(sessionStorage.getItem(environment.TOKEN_NAME));
    const decodedToken = helper.decodeToken(token);
    //console.log(decodedToken);
    this.usuario = decodedToken.user_name;

    this.menuService.listarPorUsuario(this.usuario).subscribe(data => {
      this.menuService.setMenuCambio(data);
    });

    // this.consultaservice.listar().subscribe(data =>{
    //   this.reg=data;
    //   console.log('hola');
    //   console.log(data);
    //   console.log(this.reg);
    // })
  }

}
