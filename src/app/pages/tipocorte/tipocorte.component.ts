import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { TipoCorte } from '../../_model/tipocorte';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TipocorteService } from '../../_service/tipocorte.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TipocorteDialogComponent } from './tipocorte-dialog/tipocorte-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipocorte',
  templateUrl: './tipocorte.component.html',
  styleUrls: ['./tipocorte.component.css']
})
export class TipocorteComponent implements OnInit {


  displayedColumns = ['idtipocorte', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<TipoCorte>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tipocorteService: TipocorteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.tipocorteService.getTipoCorteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tipocorteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.tipocorteService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: TipoCorte[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(tipocorte?: TipoCorte) {
    this.dialog.open(TipocorteDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: tipocorte
    });
  }

  eliminar(tipocorte: TipoCorte) {
    this.tipocorteService.eliminar(tipocorte.idtipocorte).pipe(switchMap(() => {
      return this.tipocorteService.listar();
    }))
      .subscribe(data => {
        this.tipocorteService.setMensajeCambio("SE ELIMINO");
        this.tipocorteService.setTipoCorteCambio(data);
      });
  }

}
