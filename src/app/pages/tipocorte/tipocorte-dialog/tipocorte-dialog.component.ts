import { Component, OnInit, Inject } from '@angular/core';
import { TipoCorte } from '../../../_model/tipocorte';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TipocorteService } from '../../../_service/tipocorte.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipocorte-dialog',
  templateUrl: './tipocorte-dialog.component.html',
  styleUrls: ['./tipocorte-dialog.component.css']
})
export class TipocorteDialogComponent implements OnInit {


  tipocorte: TipoCorte;

  constructor(
    private dialogRef: MatDialogRef<TipocorteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: TipoCorte,
    private tipocorteService : TipocorteService,
  ) { }

  ngOnInit(): void {
    this.tipocorte = { ...this.data };
  }

  operar() {
    if (this.tipocorte.descripcion!='') {
      if (this.tipocorte != null && this.tipocorte.idtipocorte > 0 ) {
        //MODIFICAR
        this.tipocorteService.modificar(this.tipocorte).pipe(switchMap(() => {
          return this.tipocorteService.listar();
        }))
        .subscribe(data => {
          this.tipocorteService.setTipoCorteCambio(data);
          this.tipocorteService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tipocorteService.registrar(this.tipocorte).subscribe(() => {
          this.tipocorteService.listar().subscribe(data => {
            this.tipocorteService.setTipoCorteCambio(data);
            this.tipocorteService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tipocorteService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar() {
    this.dialogRef.close();
  }

}
