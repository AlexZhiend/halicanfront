import { Component, OnInit, ViewChild } from '@angular/core';
import { Moneda } from '../../_model/moneda';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MonedaService } from '../../_service/moneda.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MonedaDialogComponent } from './moneda-dialog/moneda-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-moneda',
  templateUrl: './moneda.component.html',
  styleUrls: ['./moneda.component.css']
})
export class MonedaComponent implements OnInit {

  displayedColumns = ['idmoneda', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<Moneda>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private monedaService: MonedaService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.monedaService.getMonedaCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.monedaService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.monedaService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Moneda[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(moneda?: Moneda) {
    this.dialog.open(MonedaDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: moneda
    });
  }

  eliminar(moneda: Moneda) {
    this.monedaService.eliminar(moneda.idmoneda).pipe(switchMap(() => {
      return this.monedaService.listar();
    }))
      .subscribe(data => {
        this.monedaService.setMensajeCambio("SE ELIMINO");
        this.monedaService.setMonedaCambio(data);
      });
  }
}
