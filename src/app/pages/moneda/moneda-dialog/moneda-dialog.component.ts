import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MonedaComponent } from '../moneda.component';
import { Moneda } from '../../../_model/moneda';
import { MonedaService } from '../../../_service/moneda.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-moneda-dialog',
  templateUrl: './moneda-dialog.component.html',
  styleUrls: ['./moneda-dialog.component.css']
})
export class MonedaDialogComponent implements OnInit {


  moneda: Moneda;

  constructor(
    private dialogRef: MatDialogRef<MonedaComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Moneda,
    private monedaService : MonedaService,
  ) { }

  ngOnInit(): void {
    this.moneda = { ...this.data };
  }

  operar() {
    if (this.moneda.descripcion!='') {
      if (this.moneda != null && this.moneda.idmoneda > 0 ) {
        //MODIFICAR
        this.monedaService.modificar(this.moneda).pipe(switchMap(() => {
          return this.monedaService.listar();
        }))
        .subscribe(data => {
          this.monedaService.setMonedaCambio(data);
          this.monedaService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.monedaService.registrar(this.moneda).subscribe(() => {
          this.monedaService.listar().subscribe(data => {
            this.monedaService.setMonedaCambio(data);
            this.monedaService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.monedaService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar() {
    this.dialogRef.close();
  }
}
