import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Producto } from '../../_model/producto';
import { ProductoService } from '../../_service/producto.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { ProductoDialogComponent } from './producto-dialog/producto-dialog.component';
import * as moment from 'moment';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  dataSource: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['codigobarras', 'nombre', 'cantidad',
 'fechavencimiento', 'presentacion','precioingreso', 'valorunitario','loteproducto','acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  condition1=1;
  condition2=2;
  condition3=3;

  constructor(
    private productoService: ProductoService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.productoService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.productoService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.productoService.getProductoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.productoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.productoService.eliminar(id).pipe(switchMap( ()=> {
      return this.productoService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.productoService.setProductoCambio(data);
      this.productoService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Producto[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.productoService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(producto?: Producto) {
    this.dialog.open(ProductoDialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: producto
    });
  }

  vencimiento(e:Date){
    var fechaactual = new Date();
    let mesEnMilisegundos = 1000 * 60 * 60 * 24 * 30;
    var m = moment(e).toDate();
    if (m.getTime()<=fechaactual.getTime()) {
      return 1;

    } else if (m.getTime()>fechaactual.getTime() &&
     m.getTime()<fechaactual.getTime()+mesEnMilisegundos){

      return 2;
    }
    else{
      return 3
    }
  }

}
