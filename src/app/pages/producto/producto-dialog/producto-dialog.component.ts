import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Producto } from '../../../_model/producto';
import { TipoAfectacion } from '../../../_model/tipoafectacion';
import { Observable, switchMap } from 'rxjs';
import { TipoafectacionService } from '../../../_service/tipoafectacion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductoService } from '../../../_service/producto.service';
import { map } from 'rxjs/operators';
import { UnidadService } from '../../../_service/unidad.service';
import { Proveedor } from '../../../_model/proveedor';
import { ProveedorService } from '../../../_service/proveedor.service';
import { CategoriaProductoService } from '../../../_service/categoria-producto.service';
import { Unidad } from 'src/app/_model/unidad';
import { CategoriaProducto } from '../../../_model/categoriaproducto';
import * as moment from 'moment';

@Component({
  selector: 'app-producto-dialog',
  templateUrl: './producto-dialog.component.html',
  styleUrls: ['./producto-dialog.component.css']
})
export class ProductoDialogComponent implements OnInit {

  maxFecha: Date = new Date();
  producto: Producto;
  proveedores: Proveedor[]=[];

  form:FormGroup;
  MycontrolTipo = new FormControl();


  tipoafectacion= new TipoAfectacion();


  fechaingreso: Date;
  fechavencimiento: Date;

  tipoafectacionSeleccionado:TipoAfectacion;
  unidadSeleccionado:Unidad;
  categoriaproductoSeleccionado:CategoriaProducto;
  proveedorseleccionado:Proveedor;

  proveedorFiltrados$: Observable<Proveedor[]>;

  // select
  tipoafectaciones: TipoAfectacion[]=[];
  unidades: Unidad[]=[];
  categoriaproductos: CategoriaProducto[]=[];

  proveedores$: Observable<Proveedor[]>;
  fecha2= new Date();
  fecha3= new Date();

  subcategorias=['COMIDA','ACCESORIOS','JUGUETES','ROPA','LIMPIEZA',
  'VACUNAS','MEDICAMENTO','CORTES','BAÑOS','DESPARASITACION','INTERVENCIONES','EXAMENES','OTROS']

  subSeleccionado:string;

  constructor(private tipoafectacionService: TipoafectacionService,
              private unidadservice:UnidadService,
              private proveedorService:ProveedorService,
              private categoriaproductoservice: CategoriaProductoService,
              public dialogRef: MatDialogRef<ProductoDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Producto,
              private productoservice: ProductoService) {
                this.form=new FormGroup({
                  'proveedores': this.MycontrolTipo,
                  'id':new FormControl(),
                  'codigobarras':new FormControl(),
                  'nombre':new FormControl(),
                  'fechavencimiento':new FormControl(),
                  'cantidad':new FormControl(),
                  'stockinicial':new FormControl(),
                  'presentacion':new FormControl(),
                  'precioingreso':new FormControl(),
                  'marcaproducto':new FormControl(),
                  'loteproducto':new FormControl(),
                  'fechaingreso':new FormControl(),
                  'valorunitario':new FormControl(),
                  'tipoafectacion':new FormControl(),
                  'unidad':new FormControl(),
                  'categoriaproducto':new FormControl(),
                  'subcategoria': new FormControl()
                });
   }

  ngOnInit(): void {
    this.listarTipoAfectacion();
    this.listarProveedor();
    this.listarUnidad();
    this.listarCategoria();

    this.producto = { ...this.data };

    if(this.producto.idproducto!= null){
      this.form.patchValue({
        id:this.data.idproducto,
        codigobarras:this.data.codigobarras,
        nombre:this.data.nombre,
        fechavencimiento:this.data.fechavencimiento,
        cantidad:this.data.cantidad,
        stockinicial:this.data.stockinicial,
        presentacion:this.data.presentacion,
        precioingreso:this.data.precioingreso,
        marcaproducto:this.data.marcaproducto,
        loteproducto:this.data.loteproducto,
        fechaingreso:this.data.fechaingreso,
        valorunitario:this.data.valorunitario,
        unidad:this.data.unidad.idunidad,
        tipoafectacion:this.data.tipoafectacion.idtipoafectacion,
        categoriaproducto:this.data.categoriaproducto.idcategoriaproducto,
        subcategoria:this.data.subcategoria
      });
      this.MycontrolTipo.setValue(this.data.proveedor);
      this.proveedorseleccionado=this.data.proveedor;
    }


    this.proveedorFiltrados$ = this.MycontrolTipo.valueChanges.pipe(
      map(val => this.filtrarProveedor(val)));
  }

  listarTipoAfectacion() {
    this.tipoafectacionService.listar().subscribe(data =>{
      this.tipoafectaciones=data;
    });
  }

  listarUnidad() {
    this.unidadservice.listar().subscribe(data =>{
      this.unidades=data;
    });
  }

  listarCategoria() {
    this.categoriaproductoservice.listar().subscribe(data =>{
      this.categoriaproductos=data;
    });
  }

  listarProveedor(){
    this.proveedorService.listar().subscribe(data =>{
      this.proveedores=data;
    });
  }

  mostrarProveedor(val: any) {
    return val ? `${val.nombre}` : val;
  }

  filtrarProveedor(val: any) {
    if (val != null && val.idproveedor > 0) {
      return this.proveedores.filter(option =>
        option.nombre.toLowerCase().includes(val.nombre.toLowerCase()) ||
        option.idproveedor.toString().includes(val.idproveedor));
    } else {
      return this.proveedores.filter(option =>
        option.nombre.toLowerCase().includes(val?.toLowerCase()) ||
        option.idproveedor.toString().includes(val));
    }
  }

  aceptar(){
    let producto1 = new Producto();
    producto1.idproducto=this.form.value['id']
    producto1.nombre = this.form.value['nombre'];
    producto1.codigobarras = this.form.value['codigobarras'];

    this.fecha2 = new Date(this.form.value['fechavencimiento']);
    var tzoffset = (this.fecha2).getTimezoneOffset()*60000;
    var localisotime=(new Date(this.fecha2.getTime()-tzoffset));
    producto1.fechavencimiento = localisotime.toISOString();

    this.fecha3 = new Date(this.form.value['fechaingreso']);
    var tzoffset1 = (this.fecha3).getTimezoneOffset()*60000;
    var localisotime1=(new Date(this.fecha3.getTime()-tzoffset1));
    producto1.fechaingreso = localisotime1.toISOString();

    // producto1.fechavencimiento = moment(this.form.value['fechavencimiento']).format('YYYY-MM-DD');
    // producto1.fechaingreso = moment(this.form.value['fechaingreso']).format('YYYY-MM-DD');
    producto1.cantidad = this.form.value['cantidad'];
    producto1.stockinicial = this.form.value['stockinicial'];
    producto1.presentacion = this.form.value['presentacion'];
    producto1.precioingreso = this.form.value['precioingreso'];
    producto1.marcaproducto = this.form.value['marcaproducto'];
    producto1.loteproducto = this.form.value['loteproducto'];
    producto1.valorunitario = this.form.value['valorunitario'];
    producto1.tipoafectacion.idtipoafectacion = this.form.value['tipoafectacion'];
    producto1.unidad.idunidad = this.form.value['unidad'];
    producto1.proveedor = this.form.value['proveedores'];
    producto1.categoriaproducto.idcategoriaproducto = this.form.value['categoriaproducto'];
    producto1.subcategoria=this.form.value['subcategoria'];

    this.producto=producto1;
    console.log(this.producto)
    if (this.form.valid == true) {
      if (this.producto != null && this.producto.idproducto > 0 ) {
        //MODIFICAR
        this.productoservice.modificar(this.producto).pipe(switchMap(() => {
          return this.productoservice.listar()
        }))
        .subscribe(data => {
          this.productoservice.setProductoCambio(data);
          this.productoservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.productoservice.registrar(this.producto).subscribe(() => {
          this.productoservice.listar().subscribe(data => {
            this.productoservice.setProductoCambio(data);
            this.productoservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.productoservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar(){
    this.dialogRef.close();
  }


  seleccionarTipoAf(e:any){

  }



}
