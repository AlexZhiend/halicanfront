import { Component, OnInit, ViewChild } from '@angular/core';
import { Hospitalizacion } from '../../_model/hospitalizacion';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HospitalizacionService } from '../../_service/hospitalizacion.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { HospdialogComponent } from './hospdialog/hospdialog.component';

@Component({
  selector: 'app-hospitalizacion',
  templateUrl: './hospitalizacion.component.html',
  styleUrls: ['./hospitalizacion.component.css']
})
export class HospitalizacionComponent implements OnInit {


  dataSource: MatTableDataSource<Hospitalizacion>;
  displayedColumns: string[] = ['idhospitalizacion', 'paciente',
  'propietario','telefono','motivoingreso','fechaingreso', 'acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private hospitalizacionService: HospitalizacionService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.hospitalizacionService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.hospitalizacionService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.hospitalizacionService.getHospitalizacionCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.hospitalizacionService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.hospitalizacionService.eliminar(id).pipe(switchMap( ()=> {
      return this.hospitalizacionService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.hospitalizacionService.setHospitalizacionCambio(data);
      this.hospitalizacionService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Hospitalizacion[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.hospitalizacionService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(hospitalizacion?: Hospitalizacion) {
    this.dialog.open(HospdialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: hospitalizacion
    });
  }
}
