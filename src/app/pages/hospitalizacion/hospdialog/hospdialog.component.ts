import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormControl } from '@angular/forms';
import { Hospitalizacion } from '../../../_model/hospitalizacion';
import { HospitalizacionService } from '../../../_service/hospitalizacion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-hospdialog',
  templateUrl: './hospdialog.component.html',
  styleUrls: ['./hospdialog.component.css']
})
export class HospdialogComponent implements OnInit {

  panelOpenState=false;
  @ViewChild(MatAccordion) accordion:MatAccordion;

  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha3= new Date();


  hospitalizacion= new Hospitalizacion();

  constructor(private hospitalizacionService:HospitalizacionService,
    public dialogRef: MatDialogRef<HospdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Hospitalizacion,
    private pacienteanimalservice:PacienteanimalService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'fechaingreso': this.fecha1,
        'horaingreso': new FormControl(),
        'motivoingreso': new FormControl(),
        'diagnostico': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {

    this.hospitalizacion = { ...this.data };

    if(this.hospitalizacion.idhospitalizacion!= null){
      this.form.patchValue({
        id:this.data.idhospitalizacion,
        fechaingreso:this.data.fechaingreso,
        horaingreso:this.data.horaingreso,
        motivoingreso:this.data.motivoingreso,
        paciente:this.data.pacienteanimal.nombre,
        diagnostico:this.data.diagnostico,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fechaingreso)
    }

  }

  aceptar(){
    let hospitalizacion1 = new Hospitalizacion();
    hospitalizacion1.idhospitalizacion=this.form.value['id']
    hospitalizacion1.observaciones = this.form.value['observaciones'];
    hospitalizacion1.motivoingreso = this.form.value['motivoingreso'];
    hospitalizacion1.diagnostico = this.form.value['diagnostico'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    hospitalizacion1.fechaingreso = localisotime.toISOString();

    hospitalizacion1.horaingreso = this.form.value['horaingreso'];

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    hospitalizacion1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;


    this.hospitalizacion=hospitalizacion1;
    console.log(this.hospitalizacion)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.hospitalizacion != null && this.hospitalizacion.idhospitalizacion > 0 ) {
        //MODIFICAR
        this.hospitalizacionService.modificar(this.hospitalizacion).pipe(switchMap(() => {
          return this.hospitalizacionService.listar()
        }))
        .subscribe(data => {
          this.hospitalizacionService.setHospitalizacionCambio(data);
          console.log(data)
          this.hospitalizacionService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.hospitalizacionService.registrar(this.hospitalizacion).subscribe(() => {
          this.hospitalizacionService.listar().subscribe(data => {
            this.hospitalizacionService.setHospitalizacionCambio(data);
            this.hospitalizacionService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.hospitalizacionService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }
}
