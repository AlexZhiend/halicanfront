import { Component, OnInit, HostBinding } from '@angular/core';
import { Menu } from 'src/app/_model/menu';
import { MenuService } from 'src/app/_service/menu.service';
import { Router } from '@angular/router';
import { OverlayContainer } from '@angular/cdk/overlay';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  @HostBinding('class') componentCssClass:any;

  menus: Menu[]=[];

  constructor(
    private menuService: MenuService,
    private router:Router,
    public overlaycontainer: OverlayContainer
  ) { }

  ngOnInit(): void {
    this.menuService.getMenuCambio().subscribe(data => {
      this.menus = data;
    })
  }

  cerrarSesion(){
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

  public onSetTheme(e:string){
    this.overlaycontainer.getContainerElement().classList.add(e);
    localStorage.setItem('theme',e);
    this.componentCssClass = e;
  }
}
