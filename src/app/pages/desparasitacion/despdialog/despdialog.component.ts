import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatTableDataSource } from '@angular/material/table';
import { Producto } from '../../../_model/producto';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RegistroDesparasitacion } from '../../../_model/registrodesparasitacion';
import { RegistrodesparasitacionService } from '../../../_service/registrodesparasitacion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { ProductoService } from '../../../_service/producto.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-despdialog',
  templateUrl: './despdialog.component.html',
  styleUrls: ['./despdialog.component.css']
})
export class DespdialogComponent implements OnInit {

  dataSource: MatTableDataSource<PacienteAnimal>;
  dataSource2: MatTableDataSource<Producto>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  displayedColumns2: string[] = ['codigo','vacuna','lote', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  desparasitador= new Producto();
  vacunas:Producto[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha2=new FormControl(this.maxFecha);
  fecha3= new Date();
  fecha4= new Date();

  registroDesparasitacion= new RegistroDesparasitacion();


  constructor(private registrodesparasitacionService:RegistrodesparasitacionService,
    public dialogRef: MatDialogRef<DespdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RegistroDesparasitacion,
    private pacienteanimalservice:PacienteanimalService,
    private productoService:ProductoService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'fechaaplicacion': this.fecha1,
        'fechasiguiente': this.fecha2,
        'peso': new FormControl(),
        'cantidad': new FormControl(),
        'medicamento': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {
    this.listarvacunas();

    this.registroDesparasitacion = { ...this.data };

    if(this.registroDesparasitacion.idregdesparasitacion!= null){
      this.form.patchValue({
        id:this.data.idregdesparasitacion,
        fechasiguiente:this.data.fechasiguiente,
        fechaaplicacion:this.data.fechaaplicacion,
        peso:this.data.peso,
        paciente:this.data.pacienteanimal.nombre,
        cantidad:this.data.cantidad,
        medicamento:this.data.medicamento,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fechaaplicacion)
      this.fecha2.setValue(this.data.fechasiguiente)
    }

  }

  listarvacunas(){
    this.productoService.buscarporsubcategoria('DESPARASITACION').subscribe(data=>{
      console.log(data)
      this.dataSource2= new MatTableDataSource(data);
    })
  }

  aceptar(){
    let registrodesparasitacion1 = new RegistroDesparasitacion();
    registrodesparasitacion1.idregdesparasitacion=this.form.value['id']
    registrodesparasitacion1.observaciones = this.form.value['observaciones'];
    registrodesparasitacion1.peso = this.form.value['peso'];
    registrodesparasitacion1.cantidad = this.form.value['cantidad'];
    registrodesparasitacion1.medicamento = this.form.value['medicamento'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    registrodesparasitacion1.fechaaplicacion = localisotime.toISOString();

    this.fecha4 = new Date(this.fecha2.value);
    var tzoffset = (this.fecha4).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha4.getTime()-tzoffset));
    registrodesparasitacion1.fechasiguiente = localisotime.toISOString();

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    registrodesparasitacion1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;


    this.registroDesparasitacion=registrodesparasitacion1;
    console.log(this.registroDesparasitacion)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.registroDesparasitacion != null && this.registroDesparasitacion.idregdesparasitacion > 0 ) {
        //MODIFICAR
        this.registrodesparasitacionService.modificar(this.registroDesparasitacion).pipe(switchMap(() => {
          return this.registrodesparasitacionService.listar()
        }))
        .subscribe(data => {
          this.registrodesparasitacionService.setRegistroDesparasitacionCambio(data);
          console.log(data)
          this.registrodesparasitacionService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.registrodesparasitacionService.registrar(this.registroDesparasitacion).subscribe(() => {
          this.registrodesparasitacionService.listar().subscribe(data => {
            this.registrodesparasitacionService.setRegistroDesparasitacionCambio(data);
            this.registrodesparasitacionService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.registrodesparasitacionService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }

  agregarvacuna(row:Producto){
    this.desparasitador=row;
    this.form.patchValue({
      medicamento:this.desparasitador.nombre,
    })
    this.snakbar.open('La vacuna se seleccionó correctamente','Aviso',{duration:1000});
  }

}
