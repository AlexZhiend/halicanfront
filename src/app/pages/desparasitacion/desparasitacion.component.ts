import { Component, OnInit, ViewChild } from '@angular/core';
import { RegistroDesparasitacion } from '../../_model/registrodesparasitacion';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RegistrodesparasitacionService } from '../../_service/registrodesparasitacion.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { DespdialogComponent } from './despdialog/despdialog.component';

@Component({
  selector: 'app-desparasitacion',
  templateUrl: './desparasitacion.component.html',
  styleUrls: ['./desparasitacion.component.css']
})
export class DesparasitacionComponent implements OnInit {


  dataSource: MatTableDataSource<RegistroDesparasitacion>;
  displayedColumns: string[] = ['idregdesparasitacion', 'paciente',
  'propietario','telefono','medicamento', 'fechaaplicacion','fechasiguiente', 'acciones'];
  cantidad: number;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private registrodesparasitacionService: RegistrodesparasitacionService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.registrodesparasitacionService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.registrodesparasitacionService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.registrodesparasitacionService.getRegistroDesparasitacionCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.registrodesparasitacionService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.registrodesparasitacionService.eliminar(id).pipe(switchMap( ()=> {
      return this.registrodesparasitacionService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.registrodesparasitacionService.setRegistroDesparasitacionCambio(data);
      this.registrodesparasitacionService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: RegistroDesparasitacion[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.registrodesparasitacionService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(registrodesparasitacion?: RegistroDesparasitacion) {
    this.dialog.open(DespdialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: registrodesparasitacion
    });
  }

}
