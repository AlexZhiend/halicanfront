import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Unidad } from '../../_model/unidad';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UnidadService } from '../../_service/unidad.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UnidadDialogComponent } from './unidad-dialog/unidad-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-unidad',
  templateUrl: './unidad.component.html',
  styleUrls: ['./unidad.component.css']
})
export class UnidadComponent implements OnInit {

  displayedColumns = ['idunidad','descripcion', 'acciones'];
  dataSource: MatTableDataSource<Unidad>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private unidadService: UnidadService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.unidadService.getUnidadCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.unidadService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.unidadService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Unidad[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(unidad?: Unidad) {
    this.dialog.open(UnidadDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: unidad
    });
  }

  eliminar(unidad: Unidad) {
    this.unidadService.eliminar(unidad.idunidad).pipe(switchMap(() => {
      return this.unidadService.listar();
    }))
      .subscribe(data => {
        this.unidadService.setMensajeCambio("SE ELIMINO");
        this.unidadService.setUnidadCambio(data);
      });
  }
}
