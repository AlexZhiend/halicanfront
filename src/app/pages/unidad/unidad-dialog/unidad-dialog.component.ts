import { Component, OnInit, Inject } from '@angular/core';
import { Unidad } from '../../../_model/unidad';
import { UnidadComponent } from '../unidad.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UnidadService } from '../../../_service/unidad.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-unidad-dialog',
  templateUrl: './unidad-dialog.component.html',
  styleUrls: ['./unidad-dialog.component.css']
})
export class UnidadDialogComponent implements OnInit {

  unidad: Unidad;

  constructor(
    private dialogRef: MatDialogRef<UnidadComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Unidad,
    private unidadService : UnidadService,
  ) { }

  ngOnInit(): void {
    this.unidad = { ...this.data };
  }

  operar() {
    if (this.unidad.descripcion!='') {
      if (this.unidad != null && this.unidad.idunidad > 0 ) {
        //MODIFICAR
        this.unidadService.modificar(this.unidad).pipe(switchMap(() => {
          return this.unidadService.listar();
        }))
        .subscribe(data => {
          this.unidadService.setUnidadCambio(data);
          this.unidadService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.unidadService.registrar(this.unidad).subscribe(() => {
          this.unidadService.listar().subscribe(data => {
            this.unidadService.setUnidadCambio(data);
            this.unidadService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.unidadService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar() {
    this.dialogRef.close();
  }

}
