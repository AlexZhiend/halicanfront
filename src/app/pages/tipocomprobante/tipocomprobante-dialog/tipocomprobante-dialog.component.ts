import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Serie } from '../../../_model/serie';
import { SerieService } from '../../../_service/serie.service';
import { TipoComprobante } from '../../../_model/tipocomprobante';
import { TipocomprobanteService } from 'src/app/_service/tipocomprobante.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipocomprobante-dialog',
  templateUrl: './tipocomprobante-dialog.component.html',
  styleUrls: ['./tipocomprobante-dialog.component.css']
})
export class TipocomprobanteDialogComponent implements OnInit {

  tipocomprobante:TipoComprobante;
  form:FormGroup;

  constructor(private dialogRef:MatDialogRef<TipocomprobanteDialogComponent>,
              private tipocomprobanteservice:TipocomprobanteService,
              @Inject(MAT_DIALOG_DATA) public data: TipoComprobante,) {
                this.form= new FormGroup({
                  'id':new FormControl(0),
                  'nomenclatura': new FormControl(''),
                  'codigo': new FormControl('')
                })
               }

  ngOnInit(): void {
    this.tipocomprobante = { ...this.data };
    if(this.tipocomprobante.idtipocomprobante!= null){
      this.form.patchValue({
        id:this.data.idtipocomprobante,
        codigo:this.data.codigotipocomprobante,
        nomenclatura:this.data.nomenclatura,
      });

    }
  }

  aceptar(){

    this.tipocomprobante.nomenclatura=this.form.value['nomenclatura'];
    this.tipocomprobante.codigotipocomprobante=this.form.value['codigo'];
    console.log(this.tipocomprobante)

    if (this.form.valid==true) {
      if (this.tipocomprobante != null && this.tipocomprobante.idtipocomprobante > 0 ) {
        //MODIFICAR
        this.tipocomprobanteservice.modificar(this.tipocomprobante).pipe(switchMap(() => {
          return this.tipocomprobanteservice.listar()
        }))
        .subscribe(data => {
          this.tipocomprobanteservice.setTipoComprobanteCambio(data);
          this.tipocomprobanteservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.tipocomprobanteservice.registrar(this.tipocomprobante).subscribe(() => {
          this.tipocomprobanteservice.listar().subscribe(data => {
            this.tipocomprobanteservice.setTipoComprobanteCambio(data);
            this.tipocomprobanteservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.tipocomprobanteservice.setMensajeCambio("Ingrese el nombre del tipo");
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}
