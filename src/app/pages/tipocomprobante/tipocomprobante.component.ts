import { Component, OnInit, ViewChild } from '@angular/core';
import { TipoComprobante } from '../../_model/tipocomprobante';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { TipocomprobanteService } from '../../_service/tipocomprobante.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TipocomprobanteDialogComponent } from './tipocomprobante-dialog/tipocomprobante-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-tipocomprobante',
  templateUrl: './tipocomprobante.component.html',
  styleUrls: ['./tipocomprobante.component.css']
})
export class TipocomprobanteComponent implements OnInit {

  displayedColumns = ['codigo','nomenclatura', 'acciones'];
  dataSource: MatTableDataSource<TipoComprobante>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private tipocomprobanteService: TipocomprobanteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.tipocomprobanteService.getTipoComprobanteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.tipocomprobanteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.tipocomprobanteService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: TipoComprobante[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(tipocomprobante?: TipoComprobante) {
    this.dialog.open(TipocomprobanteDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: tipocomprobante
    });
  }

  eliminar(tipocomprobante: TipoComprobante) {
    this.tipocomprobanteService.eliminar(tipocomprobante.idtipocomprobante).pipe(switchMap(() => {
      return this.tipocomprobanteService.listar();
    }))
      .subscribe(data => {
        this.tipocomprobanteService.setMensajeCambio("SE ELIMINO");
        this.tipocomprobanteService.setTipoComprobanteCambio(data);
      });
  }
}
