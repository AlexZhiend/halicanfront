import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExamenMedico } from '../../../_model/examenmedico';
import { ExamenmedicoComponent } from '../examenmedico.component';
import { ExamenmedicoService } from '../../../_service/examenmedico.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-examenmedico-dialog',
  templateUrl: './examenmedico-dialog.component.html',
  styleUrls: ['./examenmedico-dialog.component.css']
})
export class ExamenmedicoDialogComponent implements OnInit {


  examenmedico: ExamenMedico;

  constructor(
    private dialogRef: MatDialogRef<ExamenmedicoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: ExamenMedico,
    private examenmedicoService : ExamenmedicoService,
  ) { }

  ngOnInit(): void {
    this.examenmedico = { ...this.data };
  }

  operar() {
    if (this.examenmedico.descripcion!=null) {
      if (this.examenmedico != null && this.examenmedico.idexamen > 0 ) {
        //MODIFICAR
        this.examenmedicoService.modificar(this.examenmedico).pipe(switchMap(() => {
          return this.examenmedicoService.listar();
        }))
        .subscribe(data => {
          this.examenmedicoService.setExamenMedicoCambio(data);
          this.examenmedicoService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.examenmedicoService.registrar(this.examenmedico).subscribe(() => {
          this.examenmedicoService.listar().subscribe(data => {
            this.examenmedicoService.setExamenMedicoCambio(data);
            this.examenmedicoService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.examenmedicoService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }



  }

  cerrar() {
    this.dialogRef.close();
  }

}
