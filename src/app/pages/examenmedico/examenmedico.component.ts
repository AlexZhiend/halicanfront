import { Component, OnInit, ViewChild } from '@angular/core';
import { ExamenMedico } from '../../_model/examenmedico';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ExamenmedicoService } from '../../_service/examenmedico.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExamenmedicoDialogComponent } from './examenmedico-dialog/examenmedico-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-examenmedico',
  templateUrl: './examenmedico.component.html',
  styleUrls: ['./examenmedico.component.css']
})
export class ExamenmedicoComponent implements OnInit {

  displayedColumns = ['idexamenmedico', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<ExamenMedico>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private examenmedicoService: ExamenmedicoService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.examenmedicoService.getExamenMedicoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.examenmedicoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.examenmedicoService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: ExamenMedico[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(examenmedico?: ExamenMedico) {
    this.dialog.open(ExamenmedicoDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: examenmedico
    });
  }

  eliminar(examenmedico: ExamenMedico) {
    this.examenmedicoService.eliminar(examenmedico.idexamen).pipe(switchMap(() => {
      return this.examenmedicoService.listar();
    }))
      .subscribe(data => {
        this.examenmedicoService.setMensajeCambio("SE ELIMINO");
        this.examenmedicoService.setExamenMedicoCambio(data);
      });
  }

}
