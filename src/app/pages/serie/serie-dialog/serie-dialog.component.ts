import { Component, OnInit, Inject } from '@angular/core';
import { Serie } from '../../../_model/serie';
import { TipoComprobante } from '../../../_model/tipocomprobante';
import { FormGroup, FormControl } from '@angular/forms';
import { TipocomprobanteService } from '../../../_service/tipocomprobante.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SerieService } from '../../../_service/serie.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-serie-dialog',
  templateUrl: './serie-dialog.component.html',
  styleUrls: ['./serie-dialog.component.css']
})
export class SerieDialogComponent implements OnInit {

  serie: Serie;

  form:FormGroup;

  tipocomprobante= new TipoComprobante();
  tipocomprobanteSeleccionado:TipoComprobante;

  // select
  tipocomprobantes: TipoComprobante[]=[];


  constructor(private tipocomprobanteservice: TipocomprobanteService,
              public dialogRef: MatDialogRef<SerieDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Serie,
              private serieservice: SerieService) {
                this.form=new FormGroup({
                  'id':new FormControl(0),
                  'serie':new FormControl(''),
                  'correlativo':new FormControl(0),
                  'tipocomprobante': new FormControl(),
                });
   }

  ngOnInit(): void {
    this.listarTipoAfectacion();

    this.serie = { ...this.data };

    if(this.serie.idserie!= null){
      this.form.patchValue({
        id:this.data.idserie,
        serie:this.data.serie,
        correlativo:this.data.correlativo,
        tipocomprobante:this.data.tipoComprobante.idtipocomprobante
      });
    }

  }

  listarTipoAfectacion() {
    this.tipocomprobanteservice.listar().subscribe(data =>{
      this.tipocomprobantes=data;
    });
  }

  aceptar(){
    let serie1 = new Serie();
    serie1.idserie=this.form.value['id']
    serie1.serie = this.form.value['serie'];
    serie1.correlativo = this.form.value['correlativo'];
    serie1.tipoComprobante.idtipocomprobante = this.form.value['tipocomprobante'];

    this.serie=serie1;
    console.log(this.serie)
    if (this.form.valid == true) {
      if (this.serie != null && this.serie.idserie > 0 ) {
        //MODIFICAR
        this.serieservice.modificar(this.serie).pipe(switchMap(() => {
          return this.serieservice.listar()
        }))
        .subscribe(data => {
          this.serieservice.setSerieCambio(data);
          this.serieservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.serieservice.registrar(this.serie).subscribe(() => {
          this.serieservice.listar().subscribe(data => {
            this.serieservice.setSerieCambio(data);
            this.serieservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.serieservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar(){
    this.dialogRef.close();
  }


}
