import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Serie } from '../../_model/serie';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SerieService } from '../../_service/serie.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SerieDialogComponent } from './serie-dialog/serie-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.css']
})
export class SerieComponent implements OnInit {


  displayedColumns = ['idserie', 'serie','correlativo','nomenclatura', 'acciones'];
  dataSource: MatTableDataSource<Serie>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private serieService: SerieService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.serieService.getSerieCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.serieService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.serieService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: Serie[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(serie?: Serie) {
    this.dialog.open(SerieDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: serie
    });
  }

  eliminar(serie: Serie) {
    this.serieService.eliminar(serie.idserie).pipe(switchMap(() => {
      return this.serieService.listar();
    }))
      .subscribe(data => {
        this.serieService.setMensajeCambio("SE ELIMINO");
        this.serieService.setSerieCambio(data);
      });
  }
}
