import { Component, OnInit, ViewChild } from '@angular/core';
import { BaniosdialogComponent } from './baniosdialog/baniosdialog.component';
import { RegistroDeBanio } from '../../_model/registrodebanio';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RegistrobanioService } from 'src/app/_service/registrobanio.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-banios',
  templateUrl: './banios.component.html',
  styleUrls: ['./banios.component.css']
})
export class BaniosComponent implements OnInit {

  dataSource: MatTableDataSource<RegistroDeBanio>;
  displayedColumns: string[] = ['idregistrobanio', 'paciente','tipobanio','telefono','estado', 'fecha', 'acciones'];
  cantidad: number;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private registrodebanioService: RegistrobanioService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.registrodebanioService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.registrodebanioService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.registrodebanioService.getRegistroDeBanioCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.registrodebanioService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.registrodebanioService.eliminar(id).pipe(switchMap( ()=> {
      return this.registrodebanioService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.registrodebanioService.setRegistroDeBanioCambio(data);
      this.registrodebanioService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: RegistroDeBanio[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.registrodebanioService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(registrodebanio?: RegistroDeBanio) {
    this.dialog.open(BaniosdialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: registrodebanio
    });
  }

}
