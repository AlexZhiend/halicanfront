import { Component, OnInit, ViewChild, Inject, enableProdMode } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { RegistroDeBanio } from '../../../_model/registrodebanio';
import { TipoBanio } from 'src/app/_model/tipobanio';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, switchMap } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TipobanioService } from '../../../_service/tipobanio.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { RegistrobanioService } from 'src/app/_service/registrobanio.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { TipocorteService } from '../../../_service/tipocorte.service';
import { TipoCorte } from '../../../_model/tipocorte';
import { EstadointernoService } from '../../../_service/estadointerno.service';
import { EstadoInterno } from '../../../_model/estadointerno';

@Component({
  selector: 'app-baniosdialog',
  templateUrl: './baniosdialog.component.html',
  styleUrls: ['./baniosdialog.component.css']
})
export class BaniosdialogComponent implements OnInit {

  panelOpenState = false;
  @ViewChild(MatAccordion) accordion: MatAccordion;

  maxFecha: Date = new Date();
  registrodebanio: RegistroDeBanio;
  tipobanios: TipoBanio[]=[];
  tipocortes: TipoCorte[]=[];
  estadosinternos: EstadoInterno[]=[];

  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];

  form:FormGroup;
  MycontrolTipo = new FormControl();

  fechaingreso:Date=new Date();
  fecha1=new FormControl(this.maxFecha);
  fecha2= new Date();

  tipobanioseleccionado:TipoBanio;

  tipobanioFiltrados$: Observable<TipoBanio[]>;
  tipobanios$: Observable<TipoBanio[]>;


  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  nombreodni: string='';
  nombreanimal: string='';


  constructor(private tipobanioService:TipobanioService,
              public dialogRef: MatDialogRef<BaniosdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: RegistroDeBanio,
              private pacienteanimalservice:PacienteanimalService,
              private registrodebanioservice: RegistrobanioService,
              private tipocorteservice: TipocorteService,
              private estadosservice:EstadointernoService,
              private snakbar:MatSnackBar) {
                this.form=new FormGroup({
                  'tipobanio': this.MycontrolTipo,
                  'id':new FormControl(),
                  'estadovisual':new FormControl(),
                  'observaciones':new FormControl(),
                  'estado':new FormControl(),
                  'tipocorte':new FormControl(),
                  'paciente': new FormControl(),
                  'fecha': this.fecha1
                });
   }

  ngOnInit(): void {
    this.listarTipobanio();
    this.listarTipocorte();
    this.listarestado();

    this.registrodebanio = { ...this.data };

    if(this.registrodebanio.idregistrobanio!= null&&this.registrodebanio.tipocorte==null){
      this.form.patchValue({
        id:this.data.idregistrobanio,
        estadovisual:this.data.estadovisual,
        observaciones:this.data.observaciones,
        estado:this.data.estado,
        paciente:this.data.pacienteanimal.nombre,
        fecha:this.data.fecha,
        });
      this.MycontrolTipo.setValue(this.data.tipobanio);
      this.tipobanioseleccionado=this.data.tipobanio;
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fecha)
    }
    else if(this.registrodebanio.idregistrobanio!= null&&this.registrodebanio.tipocorte!=null){
      this.form.patchValue({
        id:this.data.idregistrobanio,
        estadovisual:this.data.estadovisual,
        observaciones:this.data.observaciones,
        estado:this.data.estado,
        paciente:this.data.pacienteanimal.nombre,
        fecha:this.data.fecha,
        tipocorte:this.data.tipocorte.idtipocorte
        });
      this.MycontrolTipo.setValue(this.data.tipobanio);
      this.tipobanioseleccionado=this.data.tipobanio;
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fecha)
    }

    this.tipobanioFiltrados$ = this.MycontrolTipo.valueChanges.pipe(
      map(val => this.filtrarTipobanio(val)));
  }

  listarTipobanio(){
    this.tipobanioService.listar().subscribe(data =>{
      this.tipobanios=data;
    });
  }

  listarTipocorte(){
    this.tipocorteservice.listar().subscribe(data=>{
      this.tipocortes=data;
      if(this.registrodebanio.idregistrobanio== null){
        const tc=this.tipocortes.find(e => e.descripcion == 'Ninguno');
        this.form.patchValue({
          tipocorte:tc?.idtipocorte
        })
        console.log(tc?.descripcion)
      }
    })
  }

  listarestado(){
    this.estadosservice.listar().subscribe(data=>{
      this.estadosinternos=data

      if(this.registrodebanio.idregistrobanio== null){
        const estado=this.estadosinternos.find(e => e.descripcion == 'Aceptado');
        this.form.patchValue({
          estado:estado?.descripcion
        })
        console.log(estado?.descripcion)
      }

    })
  }

  mostrarTipobanio(val: any) {
    return val ? `${val.descripcion}` : val;
  }

  filtrarTipobanio(val: any) {
    if (val != null && val.idtipobanio > 0) {
      return this.tipobanios.filter(option =>
        option.descripcion.toLowerCase().includes(val.descripcion.toLowerCase()) )

    } else {
      return this.tipobanios.filter(option =>
        option.descripcion.toLowerCase().includes(val?.toLowerCase()) )
    }
  }

  aceptar(){
    let registrodebanio1 = new RegistroDeBanio();
    registrodebanio1.idregistrobanio=this.form.value['id']
    registrodebanio1.estadovisual = this.form.value['estadovisual'];
    registrodebanio1.observaciones = this.form.value['observaciones'];
    registrodebanio1.estado = this.form.value['estado'];

    this.fecha2 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha2).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha2.getTime()-tzoffset));
    registrodebanio1.fecha = localisotime;

    let pac = new PacienteAnimal();
    let tipcorte= new TipoCorte();
    pac=this.pacienteseleccionado;
    registrodebanio1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;
    registrodebanio1.tipobanio = this.form.value['tipobanio'];

    tipcorte.idtipocorte= this.form.value['tipocorte'];
    registrodebanio1.tipocorte = tipcorte;

    this.registrodebanio=registrodebanio1;
    console.log(this.registrodebanio)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.registrodebanio != null && this.registrodebanio.idregistrobanio > 0 ) {
        //MODIFICAR
        this.registrodebanioservice.modificar(this.registrodebanio).pipe(switchMap(() => {
          return this.registrodebanioservice.listar()
        }))
        .subscribe(data => {
          this.registrodebanioservice.setRegistroDeBanioCambio(data);
          console.log(data)
          this.registrodebanioservice.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.registrodebanioservice.registrar(this.registrodebanio).subscribe(() => {
          this.registrodebanioservice.listar().subscribe(data => {
            this.registrodebanioservice.setRegistroDeBanioCambio(data);
            this.registrodebanioservice.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.registrodebanioservice.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
    this.panelOpenState=false;
  }

}
