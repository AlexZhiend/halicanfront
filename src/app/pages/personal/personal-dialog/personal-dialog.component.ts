import { Component, OnInit, Inject } from '@angular/core';
import { PersonalVeterinario } from '../../../_model/personalveterinario';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PersonalveterinarioService } from '../../../_service/personalveterinario.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-personal-dialog',
  templateUrl: './personal-dialog.component.html',
  styleUrls: ['./personal-dialog.component.css']
})
export class PersonalDialogComponent implements OnInit {


  personalveterinario: PersonalVeterinario;
  maxFecha: Date = new Date();


  constructor(
    private dialogRef: MatDialogRef<PersonalDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: PersonalVeterinario,
    private personalveterinarioService : PersonalveterinarioService
  ) { }

  ngOnInit(): void {
    this.personalveterinario = { ...this.data };
  }

  operar() {
    if (this.personalveterinario.dnipersonal!='' &&
      this.personalveterinario.nombrespersonal!=''
      && this.personalveterinario.areapersonal!='') {
      if (this.personalveterinario != null && this.personalveterinario.idpersonal > 0 ) {
        //MODIFICAR
        this.personalveterinarioService.modificar(this.personalveterinario).pipe(switchMap(() => {
          return this.personalveterinarioService.listar();
        }))
        .subscribe(data => {
          this.personalveterinarioService.setPersonalVeterinarioCambio(data);
          this.personalveterinarioService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.personalveterinarioService.registrar(this.personalveterinario).subscribe(() => {
          this.personalveterinarioService.listar().subscribe(data => {
            this.personalveterinarioService.setPersonalVeterinarioCambio(data);
            this.personalveterinarioService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.personalveterinarioService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }

  }

  cerrar() {
    this.dialogRef.close();
  }
}
