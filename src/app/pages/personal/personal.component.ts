import { Component, OnInit, ViewChild } from '@angular/core';
import { PersonalVeterinario } from '../../_model/personalveterinario';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PersonalveterinarioService } from '../../_service/personalveterinario.service';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PersonalDialogComponent } from './personal-dialog/personal-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {


  displayedColumns = ['dnipersonal','nombrepersonal','telefono','area','fechanacimiento', 'acciones'];
  dataSource: MatTableDataSource<PersonalVeterinario>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private personalveterinarioService: PersonalveterinarioService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.personalveterinarioService.getPersonalVeterinarioCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.personalveterinarioService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.personalveterinarioService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: PersonalVeterinario[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(personalveterinario?: PersonalVeterinario) {
    this.dialog.open(PersonalDialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: personalveterinario
    });
  }

  eliminar(personalveterinario: PersonalVeterinario) {
    this.personalveterinarioService.eliminar(personalveterinario.idpersonal).pipe(switchMap(() => {
      return this.personalveterinarioService.listar();
    }))
      .subscribe(data => {
        this.personalveterinarioService.setMensajeCambio("SE ELIMINO");
        this.personalveterinarioService.setPersonalVeterinarioCambio(data);
      });
  }

}
