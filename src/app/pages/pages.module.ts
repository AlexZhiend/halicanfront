import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { InicioComponent } from './inicio/inicio.component';
import { Not404Component } from './not404/not404.component';
import { Not403Component } from './not403/not403.component';
import { MaterialModule } from '../material/material.module';
import { PagesRoutingModule } from "./pages-routing.module";
import { ExamenmedicoComponent } from './examenmedico/examenmedico.component';
import { TipobanioComponent } from './tipobanio/tipobanio.component';
import { TipocorteComponent } from './tipocorte/tipocorte.component';
import { EstadointernoComponent } from './estadointerno/estadointerno.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { UnidadComponent } from './unidad/unidad.component';
import { TipoafectacionComponent } from './tipoafectacion/tipoafectacion.component';
import { ProveedorComponent } from './proveedor/proveedor.component';
import { MonedaComponent } from './moneda/moneda.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { TipodocumentoComponent } from './tipodocumento/tipodocumento.component';
import { CategoriaDialogoComponent } from './categoria/categoria-dialogo/categoria-dialogo.component';
import { EmpresaDialogoComponent } from './empresa/empresa-dialogo/empresa-dialogo.component';
import { EstadointernoDialogComponent } from './estadointerno/estadointerno-dialog/estadointerno-dialog.component';
import { ExamenmedicoDialogComponent } from './examenmedico/examenmedico-dialog/examenmedico-dialog.component';
import { MonedaDialogComponent } from './moneda/moneda-dialog/moneda-dialog.component';
import { ProveedorDialogComponent } from './proveedor/proveedor-dialog/proveedor-dialog.component';
import { TipoafectacionDialogComponent } from './tipoafectacion/tipoafectacion-dialog/tipoafectacionr-dialog.component';
import { UnidadDialogComponent } from './unidad/unidad-dialog/unidad-dialog.component';
import { TipobanioDialogComponent } from './tipobanio/tipobanio-dialog/tipobanio-dialog.component';
import { TipocorteDialogComponent } from './tipocorte/tipocorte-dialog/tipocorte-dialog.component';
import { TipodocumentoDialogComponent } from './tipodocumento/tipodocumento-dialog/tipodocumento-dialog.component';
import { PersonalComponent } from './personal/personal.component';
import { PersonalDialogComponent } from './personal/personal-dialog/personal-dialog.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteDialogComponent } from './cliente/cliente-dialog/cliente-dialog.component';
import { PacienteanimalComponent } from './pacienteanimal/pacienteanimal.component';
import { ProductoComponent } from './producto/producto.component';
import { ProductoDialogComponent } from './producto/producto-dialog/producto-dialog.component';
import { VentasComponent } from './ventas/ventas.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import { BusquedaclienteComponent } from './ventas/busquedacliente/busquedacliente.component';
import { TipocomprobanteComponent } from './tipocomprobante/tipocomprobante.component';
import { TipocomprobanteDialogComponent } from './tipocomprobante/tipocomprobante-dialog/tipocomprobante-dialog.component';
import { SerieComponent } from './serie/serie.component';
import { SerieDialogComponent } from './serie/serie-dialog/serie-dialog.component';
import { ConfirmacionComponent } from './ventas/confirmacion/confirmacion.component';
import { ImpresionComponent } from './ventas/confirmacion/impresion/impresion.component';
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { ResumenventasComponent } from './resumenventas/resumenventas.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { PacientedialogComponent } from './pacienteanimal/pacientedialog/pacientedialog.component';
import { ConsultadialogComponent } from './consulta/consultadialog/consultadialog.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { BaniosComponent } from './banios/banios.component';
import { BaniosdialogComponent } from './banios/baniosdialog/baniosdialog.component';
import { VacunacionComponent } from './vacunacion/vacunacion.component';
import { VacunaciondialogComponent } from './vacunacion/vacunaciondialog/vacunaciondialog.component';
import { DesparasitacionComponent } from './desparasitacion/desparasitacion.component';
import { DespdialogComponent } from './desparasitacion/despdialog/despdialog.component';
import { HospedajeComponent } from './hospedaje/hospedaje.component';
import { HospedajedialogComponent } from './hospedaje/hospedajedialog/hospedajedialog.component';
import { HospitalizacionComponent } from './hospitalizacion/hospitalizacion.component';
import { HospdialogComponent } from './hospitalizacion/hospdialog/hospdialog.component';
import { TratamientoComponent } from './tratamiento/tratamiento.component';
import { TratamientodialogComponent } from './tratamiento/tratamientodialog/tratamientodialog.component';
import { CirujiaComponent } from './cirujia/cirujia.component';
import { CirujiadialogComponent } from './cirujia/cirujiadialog/cirujiadialog.component';

@NgModule({
  imports: [
      MaterialModule,
      CommonModule,
      HttpClientModule,
      ReactiveFormsModule,
      FormsModule,
      PagesRoutingModule,
      FlexLayoutModule,
      NgxExtendedPdfViewerModule
  ],
  exports: [],
  declarations: [
        LayoutComponent,
        LoginComponent,
        InicioComponent,
        Not404Component,
        Not403Component,
        ExamenmedicoComponent,
        TipobanioComponent,
        TipocorteComponent,
        EstadointernoComponent,
        CategoriaComponent,
        UnidadComponent,
        TipoafectacionComponent,
        ProveedorComponent,
        MonedaComponent,
        EmpresaComponent,
        TipodocumentoComponent,
        CategoriaDialogoComponent,
        EmpresaDialogoComponent,
        EstadointernoDialogComponent,
        ExamenmedicoDialogComponent,
        MonedaDialogComponent,
        ProveedorDialogComponent,
        TipoafectacionDialogComponent,
        UnidadDialogComponent,
        TipobanioDialogComponent,
        TipocorteDialogComponent,
        TipodocumentoDialogComponent,
        PersonalComponent,
        PersonalDialogComponent,
        ClienteComponent,
        ClienteDialogComponent,
        PacienteanimalComponent,
        ProductoComponent,
        ProductoDialogComponent,
        VentasComponent,
        BusquedaclienteComponent,
        TipocomprobanteComponent,
        TipocomprobanteDialogComponent,
        SerieComponent,
        SerieDialogComponent,
        ConfirmacionComponent,
        ImpresionComponent,
        ResumenventasComponent,
        ConsultaComponent,
        PacientedialogComponent,
        ConsultadialogComponent,
        BaniosComponent,
        BaniosdialogComponent,
        VacunacionComponent,
        VacunaciondialogComponent,
        DesparasitacionComponent,
        DespdialogComponent,
        HospedajeComponent,
        HospedajedialogComponent,
        HospitalizacionComponent,
        HospdialogComponent,
        TratamientoComponent,
        TratamientodialogComponent,
        CirujiaComponent,
        CirujiadialogComponent
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-Es'}
  ],
})
export class PagesModule { }
