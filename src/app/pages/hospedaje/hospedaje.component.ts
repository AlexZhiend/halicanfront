import { Component, OnInit, ViewChild } from '@angular/core';
import { RegistroDeHospedaje } from '../../_model/registrodehospedaje';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RegistrohospedajeService } from '../../_service/registrohospedaje.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { HospedajedialogComponent } from './hospedajedialog/hospedajedialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-hospedaje',
  templateUrl: './hospedaje.component.html',
  styleUrls: ['./hospedaje.component.css']
})
export class HospedajeComponent implements OnInit {


  dataSource: MatTableDataSource<RegistroDeHospedaje>;
  displayedColumns: string[] = ['idregistrohospedaje', 'paciente',
  'propietario','telefono','estadoanimal', 'fechaentrada','fechasalida', 'acciones'];
  cantidad: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private registrohospedajeService: RegistrohospedajeService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    /*this.registrodehospedajeService.listar().subscribe(data => {
      this.crearTabla(data);
    });*/

    this.registrohospedajeService.listarPageable(0 , 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort= this.sort;
    });

    this.registrohospedajeService.getRegistroDeHospedajeCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.registrohospedajeService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

  }

  eliminar(id: number){
    this.registrohospedajeService.eliminar(id).pipe(switchMap( ()=> {
      return this.registrohospedajeService.listar();
    }))
    .subscribe(data => {
      //this.dataSource = new MatTableDataSource(data);
      this.registrohospedajeService.setRegistroDeHospedajeCambio(data);
      this.registrohospedajeService.setMensajeCambio('SE ELIMINO');
    });
  }

  filtrar(e : any){
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: RegistroDeHospedaje[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  mostrarMas(e: any){
    this.registrohospedajeService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);

    });
  }

  abrirDialogo(registrodehospedaje?: RegistroDeHospedaje) {
    this.dialog.open(HospedajedialogComponent, {
      maxWidth: '90%',
      minWidth: '90%',
      data: registrodehospedaje
    });
  }


}
