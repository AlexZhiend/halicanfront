import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { RegistroDeHospedaje } from '../../../_model/registrodehospedaje';
import { FormControl, FormGroup } from '@angular/forms';
import { PacienteAnimal } from '../../../_model/pacienteanimal';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { RegistrohospedajeService } from '../../../_service/registrohospedaje.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PacienteanimalService } from '../../../_service/pacienteanimal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-hospedajedialog',
  templateUrl: './hospedajedialog.component.html',
  styleUrls: ['./hospedajedialog.component.css']
})
export class HospedajedialogComponent implements OnInit {

  panelOpenState=false;
  @ViewChild(MatAccordion) accordion:MatAccordion;

  dataSource: MatTableDataSource<PacienteAnimal>;
  displayedColumns: string[] = ['historia', 'nombre', 'cliente', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  pacienteseleccionado= new PacienteAnimal();
  pacientes:PacienteAnimal[]=[];
  nombreodni: string='';
  nombreanimal: string='';

  maxFecha: Date = new Date();
  form:FormGroup;
  fecha1=new FormControl(this.maxFecha);
  fecha2=new FormControl(this.maxFecha);
  fecha3= new Date();
  fecha4= new Date();

  registroDeHospedaje= new RegistroDeHospedaje();

  constructor(private registrohospedajeService:RegistrohospedajeService,
    public dialogRef: MatDialogRef<HospedajedialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RegistroDeHospedaje,
    private pacienteanimalservice:PacienteanimalService,
    private snakbar:MatSnackBar) {
      this.form=new FormGroup({
        'paciente': new FormControl(),
        'id': new FormControl(),
        'fechaentrada': this.fecha1,
        'fechasalida': this.fecha2,
        'estadoanimal': new FormControl(),
        'alimento': new FormControl(),
        'observaciones': new FormControl(),
      });
     }

  ngOnInit(): void {

    this.registroDeHospedaje = { ...this.data };

    if(this.registroDeHospedaje.idregistrohospedaje!= null){
      this.form.patchValue({
        id:this.data.idregistrohospedaje,
        fechaentrada:this.data.fechaentrada,
        fechasalida:this.data.fechasalida,
        estadoanimal:this.data.estadoanimal,
        paciente:this.data.pacienteanimal.nombre,
        alimento:this.data.alimento,
        observaciones:this.data.observaciones,
        });
      this.pacienteseleccionado=this.data.pacienteanimal;
      this.fecha1.setValue(this.data.fechaentrada)
      this.fecha2.setValue(this.data.fechasalida)
    }

  }

  aceptar(){
    let registrodehospedaje1 = new RegistroDeHospedaje();
    registrodehospedaje1.idregistrohospedaje=this.form.value['id']
    registrodehospedaje1.observaciones = this.form.value['observaciones'];
    registrodehospedaje1.estadoanimal = this.form.value['estadoanimal'];
    registrodehospedaje1.alimento = this.form.value['alimento'];

    this.fecha3 = new Date(this.fecha1.value);
    var tzoffset = (this.fecha3).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha3.getTime()-tzoffset));
    registrodehospedaje1.fechaentrada = localisotime.toISOString();

    this.fecha4 = new Date(this.fecha2.value);
    var tzoffset = (this.fecha4).getTimezoneOffset()*60000 ;
    var localisotime=(new Date(this.fecha4.getTime()-tzoffset));
    registrodehospedaje1.fechasalida = localisotime.toISOString();

    let pac = new PacienteAnimal();
    pac=this.pacienteseleccionado;
    registrodehospedaje1.pacienteanimal.idpacienteanimal = pac.idpacienteanimal;


    this.registroDeHospedaje=registrodehospedaje1;
    console.log(this.registroDeHospedaje)
    if (this.form.valid == true && this.form.value['tipobanio']!="") {
      if (this.registroDeHospedaje != null && this.registroDeHospedaje.idregistrohospedaje > 0 ) {
        //MODIFICAR
        this.registrohospedajeService.modificar(this.registroDeHospedaje).pipe(switchMap(() => {
          return this.registrohospedajeService.listar()
        }))
        .subscribe(data => {
          this.registrohospedajeService.setRegistroDeHospedajeCambio(data);
          console.log(data)
          this.registrohospedajeService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.registrohospedajeService.registrar(this.registroDeHospedaje).subscribe(() => {
          this.registrohospedajeService.listar().subscribe(data => {
            this.registrohospedajeService.setRegistroDeHospedajeCambio(data);
            this.registrohospedajeService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.registrohospedajeService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }
  }

  cerrar(){
    this.dialogRef.close();
  }

  buscarpaciente(){
    if (this.nombreanimal=='' && this.nombreodni!='') {
      this.pacienteanimalservice.buscarPorCliente(this.nombreodni).subscribe(data=>{
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort= this.sort;
        this.nombreanimal='';
        this.nombreodni='';
      })
    } else if(this.nombreodni=='' && this.nombreanimal!='') {
      this.pacienteanimalservice.buscarPorNombre(this.nombreanimal).subscribe(data1=>{
        this.dataSource = new MatTableDataSource(data1);
        this.dataSource.sort= this.sort;
        this.nombreodni='';
        this.nombreanimal='';
      })
    }else{
      this.snakbar.open('Ingrese los datos del paciente o del propietario')
    }

  }

  agregar(row: PacienteAnimal){
    this.pacienteseleccionado=row;
    this.form.patchValue({
      paciente:this.pacienteseleccionado.nombre,
    })
    this.snakbar.open('El cliente se seleccionó correctamente','Aviso',{duration:1000});
  }

}
