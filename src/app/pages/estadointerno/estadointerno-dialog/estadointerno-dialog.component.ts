import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EstadoInterno } from '../../../_model/estadointerno';
import { CategoriaDialogoComponent } from '../../categoria/categoria-dialogo/categoria-dialogo.component';
import { EstadointernoService } from '../../../_service/estadointerno.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-estadointerno-dialog',
  templateUrl: './estadointerno-dialog.component.html',
  styleUrls: ['./estadointerno-dialog.component.css']
})
export class EstadointernoDialogComponent implements OnInit {

  estadointerno: EstadoInterno;

  constructor(
    private dialogRef: MatDialogRef<CategoriaDialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: EstadoInterno,
    private estadointernoService : EstadointernoService,
  ) { }

  ngOnInit(): void {
    this.estadointerno = { ...this.data };
  }

  operar() {
    if (this.estadointerno.descripcion!=null) {
      if (this.estadointerno != null && this.estadointerno.idestadointerno > 0 ) {
        //MODIFICAR
        this.estadointernoService.modificar(this.estadointerno).pipe(switchMap(() => {
          return this.estadointernoService.listar();
        }))
        .subscribe(data => {
          this.estadointernoService.setEstadoInternoCambio(data);
          this.estadointernoService.setMensajeCambio("SE MODIFICO");
        });

      } else {
        //REGISTRAR
        this.estadointernoService.registrar(this.estadointerno).subscribe(() => {
          this.estadointernoService.listar().subscribe(data => {
            this.estadointernoService.setEstadoInternoCambio(data);
            this.estadointernoService.setMensajeCambio("SE REGISTRO");
          });
        });
      }
      this.cerrar();
    }else{
      this.estadointernoService.setMensajeCambio('Error: Ingrese todos los datos requeridos');
    }



  }

  cerrar() {
    this.dialogRef.close();
  }

}
