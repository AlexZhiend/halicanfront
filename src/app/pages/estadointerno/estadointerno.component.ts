import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EstadoInterno } from '../../_model/estadointerno';
import { EstadointernoService } from '../../_service/estadointerno.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EstadointernoDialogComponent } from './estadointerno-dialog/estadointerno-dialog.component';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-estadointerno',
  templateUrl: './estadointerno.component.html',
  styleUrls: ['./estadointerno.component.css']
})
export class EstadointernoComponent implements OnInit {


  displayedColumns = ['idestadointerno', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<EstadoInterno>= new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private estadointernoService: EstadointernoService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.estadointernoService.getEstadoInternoCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.estadointernoService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.estadointernoService.listar().subscribe(data => {
      this.crearTabla(data);
    });
  }

  filtrar(e: any) {
    this.dataSource.filter = e.target.value.trim().toLowerCase();
  }

  crearTabla(data: EstadoInterno[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  abrirDialogo(estadointerno?: EstadoInterno) {
    this.dialog.open(EstadointernoDialogComponent, {
      maxWidth: '90vw',
      minWidth: '900px',
      data: estadointerno
    });
  }

  eliminar(estadointerno: EstadoInterno) {
    this.estadointernoService.eliminar(estadointerno.idestadointerno).pipe(switchMap(() => {
      return this.estadointernoService.listar();
    }))
      .subscribe(data => {
        this.estadointernoService.setMensajeCambio("SE ELIMINO");
        this.estadointernoService.setEstadoInternoCambio(data);
      });
  }
}
