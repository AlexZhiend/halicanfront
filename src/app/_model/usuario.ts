import { Rol } from './rol';
export class Usuario {
  idUsuario=0;
  username='';
  password='';
  enabled=true;
  roles: Rol[]= [];
}
