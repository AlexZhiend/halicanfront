import { PacienteAnimal } from './pacienteanimal';
import { PersonalVeterinario } from './personalveterinario';
import { ConstantesFisiologicas } from './constantesfisiologicas';
export class Consulta{
  idconsulta:number=0;
  fecha:string;
  motivo:string='';
  inicio:string='';
  signossintomas:string='';
  receta='';
  pacienteanimal:PacienteAnimal=new PacienteAnimal();
  personalveterinario:PersonalVeterinario= new PersonalVeterinario();
  constantesfisiologicas:ConstantesFisiologicas[]= [];
}
