import { TipoDocumento } from './tipodocumento';
export class Cliente{
  idcliente:number=0;
  dni:string='';
  razonsocial:string='';
  nombresyapellidos:string='';
  departamento:string='';
  provincia:string='';
  distrito:string='';
  direccion:string='';
  telefono:string='';
  tipodocumento:TipoDocumento= new TipoDocumento();
}
