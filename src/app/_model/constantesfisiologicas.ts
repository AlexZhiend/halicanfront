import { Consulta } from './consulta';
export class ConstantesFisiologicas{
  idconstantes:number=0;
  consciente:string='';
  hidratacion:string='';
  fecha:string='';
  mucosas:string='';
  peso:number=0;
  frecuenciacardiaca:string='';
  frecuenciarespiratoria:string='';
  pulso:string='';
  tllenadocapilar:string='';
  temperaturas:string='';
  descripcionexamen:string='';
}
