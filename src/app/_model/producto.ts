import { TipoAfectacion } from './tipoafectacion';
import { Unidad } from './unidad';
import { Proveedor } from './proveedor';
import { CategoriaProducto } from './categoriaproducto';
export class Producto{
  idproducto:number=0;
  nombre:string='';
  codigobarras:string='';
  fechavencimiento:string='';
  cantidad:number=0;
  stockinicial:number=0;
  presentacion:string='';
  precioingreso:number=0;
  marcaproducto:string='';
  loteproducto:string='';
  fechaingreso:string='';
  valorunitario:number=0;
  tipoafectacion:TipoAfectacion=new TipoAfectacion();
  unidad:Unidad= new Unidad();
  proveedor:Proveedor=new Proveedor();
  categoriaproducto:CategoriaProducto = new CategoriaProducto();
  subcategoria:string;
}
