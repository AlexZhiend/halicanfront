import { PacienteAnimal } from './pacienteanimal';
export class Hospitalizacion{
  idhospitalizacion=0;
  fechaingreso='';
  horaingreso='';
  motivoingreso='';
  diagnostico='';
  observaciones='';
  pacienteanimal:PacienteAnimal= new PacienteAnimal();
}
