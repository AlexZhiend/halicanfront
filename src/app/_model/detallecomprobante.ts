import { Producto } from './producto';
export class DetalleComprobante{
  iddetallecomprobante:number=0;
  cantidad:number=0;
  item:number=0;
  descripcion:string='';
  preciounitario:number=0;
  igv:number=0;
  porcentajeigv:number=0;
  valortotal:number=0;
  importetotal:number=0;
  valorunitario:number=0;
  descuentoitem:number=0;
  producto:Producto=new Producto();
}
