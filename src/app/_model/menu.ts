import { Rol } from './rol';

export class Menu{
    idMenu: number=0;
    icono: string='';
    nombre: string='';
    url: string='';
    roles: Rol[]=[];
}
