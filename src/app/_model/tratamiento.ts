import { Hospitalizacion } from './hospitalizacion';
import { PacienteAnimal } from './pacienteanimal';
export class Tratamiento{
  idtratamiento=0;
  descripcion='';
  fechainicio='';
  fechafin='';
  estado='';
  observaciones='';
  pacienteanimal= new PacienteAnimal();
}
