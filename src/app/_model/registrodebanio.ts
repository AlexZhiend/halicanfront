import { PacienteAnimal } from './pacienteanimal';
import { TipoBanio } from './tipobanio';
import { TipoCorte } from './tipocorte';
export class RegistroDeBanio{
  idregistrobanio=0;
  fecha:Date;
  estadovisual='';
  observaciones='';
  estado='';
  tipobanio= new TipoBanio();
  tipocorte= new TipoCorte();
  pacienteanimal: PacienteAnimal= new PacienteAnimal();
}
