import { Cliente } from './cliente';
export class PacienteAnimal{
  idpacienteanimal=0;
  numerohistoria=0;
  nombre='';
  raza='';
  especie='';
  color='';
  esterilizacion:string='';
  sexo='';
  fechanacimiento='';
  cliente= new Cliente();
}
