import { PacienteAnimal } from './pacienteanimal';
export class RegistroVacunas{
  idregistrovacuna=0;
  fechaaplicacion='';
  fechasiguiente='';
  peso=0;
  cantidad=0;
  vacuna='';
  lote='';
  laboratorio='';
  observaciones='';
  pacienteanimal:PacienteAnimal= new PacienteAnimal();
}
