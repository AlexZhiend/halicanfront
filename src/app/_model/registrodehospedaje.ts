import { PacienteAnimal } from './pacienteanimal';
export class RegistroDeHospedaje{
  idregistrohospedaje=0;
  fechaentrada='';
  fechasalida='';
  estadoanimal='';
  alimento='';
  observaciones='';
  pacienteanimal=new PacienteAnimal();
}
