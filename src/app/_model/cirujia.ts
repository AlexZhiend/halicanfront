import { PacienteAnimal } from './pacienteanimal';
export class Cirujia{
  idcirujia:number;
  intervencion:string;
  fecha:Date;
  hora:string;
  observaciones:string;
  pacienteanimal: PacienteAnimal= new PacienteAnimal();
}
