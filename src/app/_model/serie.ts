import { TipoComprobante } from './tipocomprobante';
export class Serie{
  idserie:number=0;
  serie:string='';
  correlativo:number=0;
  tipoComprobante:TipoComprobante=new TipoComprobante();
}
