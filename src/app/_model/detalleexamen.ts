import { Cliente } from './cliente';
import { ExamenMedico } from './examenmedico';
export class DetalleExamen{
  iddetalleexamen=0;
  estado='';
  fecha='';
  fechaentrega='';
  examenmedico= new ExamenMedico();
  cliente=new Cliente();
}
